package com.polsl.reviewme.entity;

import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.JoinEntity;
import org.greenrobot.greendao.annotation.ToMany;

import java.util.List;

/**
 * Position entity to save information in database.
 * Created by Rafał Swoboda on 2017-09-28.
 *
 * @author Rafał Swoboda
 * @version 1.0
 * @since 03.12.2017
 */

@Entity
public class Position {
    @Id
    private Long id;
    private String title;
    private long premiere;
    @SerializedName("averageRating")
    private double average_rating;
    @ToMany
    @JoinEntity(
            entity = PositionHasGenre.class,
            sourceProperty = "positionId",
            targetProperty = "genreId"
    )
    @SerializedName("genreCollection")
    private List<Genre> genres;
    /**
     * Used to resolve relations
     */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;
    /**
     * Used for active entity operations.
     */
    @Generated(hash = 322506926)
    private transient PositionDao myDao;

    @Generated(hash = 769416127)
    public Position(Long id, String title, long premiere, double average_rating) {
        this.id = id;
        this.title = title;
        this.premiere = premiere;
        this.average_rating = average_rating;
    }

    @Generated(hash = 958937587)
    public Position() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 481268026)
    public List<Genre> getGenres() {
        if (genres == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            GenreDao targetDao = daoSession.getGenreDao();
            List<Genre> genresNew = targetDao._queryPosition_Genres(id);
            synchronized (this) {
                if (genres == null) {
                    genres = genresNew;
                }
            }
        }
        return genres;
    }

    /**
     * Resets a to-many relationship, making the next get call to query for a fresh result.
     */
    @Generated(hash = 1988821389)
    public synchronized void resetGenres() {
        genres = null;
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    public long getPremiere() {
        return this.premiere;
    }

    public void setPremiere(long premiere) {
        this.premiere = premiere;
    }

    public double getAverage_rating() {
        return this.average_rating;
    }

    public void setAverage_rating(double average_rating) {
        this.average_rating = average_rating;
    }

    /**
     * called by internal mechanisms, do not call yourself.
     */
    @Generated(hash = 741478416)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getPositionDao() : null;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Position))
            return false;
        Position other = (Position) o;
        if (this.getId().equals(other.getId()))
            return true;
        else
            return false;
    }
}
