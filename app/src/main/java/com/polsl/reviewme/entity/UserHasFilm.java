package com.polsl.reviewme.entity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

/**
 * UserHasFilm entity to save information in database.
 * Created by Rafał Swoboda on 2017-11-17.
 *
 * @author Rafał Swoboda
 * @version 1.0
 * @since 03.12.2017
 */

@Entity
public class UserHasFilm {
    @Id(autoincrement = true)
    private Long id;
    private long userId;
    private long filmId;

    public UserHasFilm(long userId, long filmId) {
        this.userId = userId;
        this.filmId = filmId;
    }

    @Generated(hash = 56106527)
    public UserHasFilm(Long id, long userId, long filmId) {
        this.id = id;
        this.userId = userId;
        this.filmId = filmId;
    }

    @Generated(hash = 87317373)
    public UserHasFilm() {
    }

    @Override
    public boolean equals(Object userHasFilm) {
        return this.getFilmId() == ((UserHasFilm) userHasFilm).getFilmId()
                && this.getUserId() == ((UserHasFilm) userHasFilm).getUserId();
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getUserId() {
        return this.userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getFilmId() {
        return this.filmId;
    }

    public void setFilmId(long filmId) {
        this.filmId = filmId;
    }
}
