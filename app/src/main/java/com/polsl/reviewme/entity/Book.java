package com.polsl.reviewme.entity;

import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.ToMany;
import org.greenrobot.greendao.annotation.ToOne;

import java.util.List;

/**
 * Book entity to save information in database.
 * Created by Rafał Swoboda on 2017-07-25.
 *
 * @author Rafał Swoboda
 * @version 1.0
 * @since 03.12.2017
 */

@Entity
public class Book {
    @Id
    private Long id;
    @ToOne(joinProperty = "positionId")
    private Position position;
    private long positionId;
    private long authorId;
    @ToMany(referencedJoinProperty = "bookId")
    private List<UserHasBook> bookUsers;
    /**
     * Used to resolve relations
     */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;
    /**
     * Used for active entity operations.
     */
    @Generated(hash = 1097957864)
    private transient BookDao myDao;

    @Generated(hash = 996212620)
    public Book(Long id, long positionId, long authorId) {
        this.id = id;
        this.positionId = positionId;
        this.authorId = authorId;
    }

    @Generated(hash = 1839243756)
    public Book() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getPositionId() {
        return this.positionId;
    }

    public void setPositionId(long positionId) {
        this.positionId = positionId;
    }

    public long getAuthorId() {
        return this.authorId;
    }

    public void setAuthorId(long authorId) {
        this.authorId = authorId;
    }

    @Generated(hash = 212624812)
    private transient Long position__resolvedKey;

    /**
     * To-one relationship, resolved on first access.
     */
    @Generated(hash = 1690304848)
    public Position getPosition() {
        long __key = this.positionId;
        if (position__resolvedKey == null || !position__resolvedKey.equals(__key)) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            PositionDao targetDao = daoSession.getPositionDao();
            Position positionNew = targetDao.load(__key);
            synchronized (this) {
                position = positionNew;
                position__resolvedKey = __key;
            }
        }
        return position;
    }

    /**
     * called by internal mechanisms, do not call yourself.
     */
    @Generated(hash = 32700486)
    public void setPosition(@NotNull Position position) {
        if (position == null) {
            throw new DaoException(
                    "To-one property 'positionId' has not-null constraint; cannot set to-one to null");
        }
        synchronized (this) {
            this.position = position;
            positionId = position.getId();
            position__resolvedKey = positionId;
        }
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    public Author getAuthor(long id) {
        AuthorDao authorDao = daoSession.getAuthorDao();
        return authorDao.load(id);
    }

    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 989653853)
    public List<UserHasBook> getBookUsers() {
        if (bookUsers == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            UserHasBookDao targetDao = daoSession.getUserHasBookDao();
            List<UserHasBook> bookUsersNew = targetDao._queryBook_BookUsers(id);
            synchronized (this) {
                if (bookUsers == null) {
                    bookUsers = bookUsersNew;
                }
            }
        }
        return bookUsers;
    }

    /**
     * Resets a to-many relationship, making the next get call to query for a fresh result.
     */
    @Generated(hash = 1617364783)
    public synchronized void resetBookUsers() {
        bookUsers = null;
    }

    /**
     * called by internal mechanisms, do not call yourself.
     */
    @Generated(hash = 1115456930)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getBookDao() : null;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Book))
            return false;
        Book other = (Book) o;
        if (this.getId().equals(other.getId()))
            return true;
        else
            return false;
    }
}
