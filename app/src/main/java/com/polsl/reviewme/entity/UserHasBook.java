package com.polsl.reviewme.entity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

/**
 * UserHasBook entity to save information in database.
 * Created by Rafał Swoboda on 2017-11-17.
 *
 * @author Rafał Swoboda
 * @version 1.0
 * @since 03.12.2017
 */

@Entity
public class UserHasBook {
    @Id(autoincrement = true)
    private Long id;
    private long userId;
    private long bookId;

    public UserHasBook(long userId, long bookId) {
        this.userId = userId;
        this.bookId = bookId;
    }

    @Generated(hash = 1204030566)
    public UserHasBook(Long id, long userId, long bookId) {
        this.id = id;
        this.userId = userId;
        this.bookId = bookId;
    }

    @Generated(hash = 1800936194)
    public UserHasBook() {
    }

    @Override
    public boolean equals(Object userHasBook) {
        return this.getUserId() == ((UserHasBook) userHasBook).getUserId()
                && this.getBookId() == ((UserHasBook) userHasBook).getBookId();
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getUserId() {
        return this.userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getBookId() {
        return this.bookId;
    }

    public void setBookId(long bookId) {
        this.bookId = bookId;
    }
}
