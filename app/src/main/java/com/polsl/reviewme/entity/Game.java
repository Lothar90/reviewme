package com.polsl.reviewme.entity;

import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.ToMany;
import org.greenrobot.greendao.annotation.ToOne;

import java.util.List;

/**
 * Game entity to save information in database.
 * Created by Rafał Swoboda on 2017-07-25.
 *
 * @author Rafał Swoboda
 * @version 1.0
 * @since 03.12.2017
 */

@Entity
public class Game {
    @Id
    private Long id;
    @ToOne(joinProperty = "positionId")
    private Position position;
    private long positionId;
    private long publisherId;
    private long companyId;
    @ToMany(referencedJoinProperty = "gameId")
    private List<UserHasGame> gameUsers;
    /**
     * Used to resolve relations
     */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;
    /**
     * Used for active entity operations.
     */
    @Generated(hash = 359416843)
    private transient GameDao myDao;

    @Generated(hash = 811128268)
    public Game(Long id, long positionId, long publisherId, long companyId) {
        this.id = id;
        this.positionId = positionId;
        this.publisherId = publisherId;
        this.companyId = companyId;
    }

    @Generated(hash = 380959371)
    public Game() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getPositionId() {
        return this.positionId;
    }

    public void setPositionId(long positionId) {
        this.positionId = positionId;
    }

    public long getPublisherId() {
        return this.publisherId;
    }

    public void setPublisherId(long publisherId) {
        this.publisherId = publisherId;
    }

    public long getCompanyId() {
        return this.companyId;
    }

    public void setCompanyId(long companyId) {
        this.companyId = companyId;
    }

    @Generated(hash = 212624812)
    private transient Long position__resolvedKey;

    /**
     * To-one relationship, resolved on first access.
     */
    @Generated(hash = 1690304848)
    public Position getPosition() {
        long __key = this.positionId;
        if (position__resolvedKey == null || !position__resolvedKey.equals(__key)) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            PositionDao targetDao = daoSession.getPositionDao();
            Position positionNew = targetDao.load(__key);
            synchronized (this) {
                position = positionNew;
                position__resolvedKey = __key;
            }
        }
        return position;
    }

    /**
     * called by internal mechanisms, do not call yourself.
     */
    @Generated(hash = 32700486)
    public void setPosition(@NotNull Position position) {
        if (position == null) {
            throw new DaoException(
                    "To-one property 'positionId' has not-null constraint; cannot set to-one to null");
        }
        synchronized (this) {
            this.position = position;
            positionId = position.getId();
            position__resolvedKey = positionId;
        }
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    public Company getCompany(long id) {
        CompanyDao companyDao = daoSession.getCompanyDao();
        return companyDao.load(id);
    }

    public Publisher getPublisher(long id) {
        PublisherDao publisherDao = daoSession.getPublisherDao();
        return publisherDao.load(id);
    }

    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 435465279)
    public List<UserHasGame> getGameUsers() {
        if (gameUsers == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            UserHasGameDao targetDao = daoSession.getUserHasGameDao();
            List<UserHasGame> gameUsersNew = targetDao._queryGame_GameUsers(id);
            synchronized (this) {
                if (gameUsers == null) {
                    gameUsers = gameUsersNew;
                }
            }
        }
        return gameUsers;
    }

    /**
     * Resets a to-many relationship, making the next get call to query for a fresh result.
     */
    @Generated(hash = 1972156534)
    public synchronized void resetGameUsers() {
        gameUsers = null;
    }

    /**
     * called by internal mechanisms, do not call yourself.
     */
    @Generated(hash = 733596598)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getGameDao() : null;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Game))
            return false;
        Game other = (Game) o;
        if (this.getId().equals(other.getId()))
            return true;
        else
            return false;
    }
}
