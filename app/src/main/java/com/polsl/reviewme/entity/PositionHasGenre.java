package com.polsl.reviewme.entity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

/**
 * PositionHasGenre entity to save information in database.
 * Created by Rafał Swoboda on 2017-11-05.
 *
 * @author Rafał Swoboda
 * @version 1.0
 * @since 03.12.2017
 */
@Entity
public class PositionHasGenre {
    @Id
    private Long id;
    private Long positionId;
    private Long genreId;

    @Generated(hash = 2129706865)
    public PositionHasGenre(Long id, Long positionId, Long genreId) {
        this.id = id;
        this.positionId = positionId;
        this.genreId = genreId;
    }

    @Generated(hash = 914932720)
    public PositionHasGenre() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPositionId() {
        return this.positionId;
    }

    public void setPositionId(Long positionId) {
        this.positionId = positionId;
    }

    public Long getGenreId() {
        return this.genreId;
    }

    public void setGenreId(Long genreId) {
        this.genreId = genreId;
    }
}
