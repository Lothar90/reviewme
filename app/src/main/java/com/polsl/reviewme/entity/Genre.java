package com.polsl.reviewme.entity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

/**
 * Genre entity to save information in database.
 * Created by Rafał Swoboda on 2017-09-28.
 *
 * @author Rafał Swoboda
 * @version 1.0
 * @since 03.12.2017
 */

@Entity
public class Genre {
    @Id
    private Long id;
    private String name;

    @Generated(hash = 1579712615)
    public Genre(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    @Generated(hash = 235763487)
    public Genre() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Genre))
            return false;
        Genre other = (Genre) o;
        if (this.getId().equals(other.getId()))
            return true;
        else
            return false;
    }

    @Override
    public String toString() {
        return getName();
    }

}
