package com.polsl.reviewme.entity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

/**
 * UserHasGame entity to save information in database.
 * Created by Rafał Swoboda on 2017-11-17.
 *
 * @author Rafał Swoboda
 * @version 1.0
 * @since 03.12.2017
 */

@Entity
public class UserHasGame {
    @Id(autoincrement = true)
    private Long id;
    private long userId;
    private long gameId;

    public UserHasGame(long userId, long gameId) {
        this.gameId = gameId;
        this.userId = userId;
    }

    @Generated(hash = 108500404)
    public UserHasGame(Long id, long userId, long gameId) {
        this.id = id;
        this.userId = userId;
        this.gameId = gameId;
    }

    @Generated(hash = 1002442997)
    public UserHasGame() {
    }

    @Override
    public boolean equals(Object userHasGame) {
        return this.getGameId() == ((UserHasGame) userHasGame).getGameId()
                && this.getUserId() == ((UserHasGame) userHasGame).getUserId();
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getGameId() {
        return this.gameId;
    }

    public void setGameId(long gameId) {
        this.gameId = gameId;
    }

    public long getUserId() {
        return this.userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }
}
