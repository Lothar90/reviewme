package com.polsl.reviewme.entity;

import java.util.List;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.internal.DaoConfig;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseStatement;
import org.greenrobot.greendao.query.Query;
import org.greenrobot.greendao.query.QueryBuilder;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table "USER_HAS_FILM".
*/
public class UserHasFilmDao extends AbstractDao<UserHasFilm, Long> {

    public static final String TABLENAME = "USER_HAS_FILM";

    /**
     * Properties of entity UserHasFilm.<br/>
     * Can be used for QueryBuilder and for referencing column names.
     */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "_id");
        public final static Property UserId = new Property(1, long.class, "userId", false, "USER_ID");
        public final static Property FilmId = new Property(2, long.class, "filmId", false, "FILM_ID");
    }

    private Query<UserHasFilm> film_FilmUsersQuery;
    private Query<UserHasFilm> loggedUser_UserFilmsQuery;

    public UserHasFilmDao(DaoConfig config) {
        super(config);
    }
    
    public UserHasFilmDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(Database db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"USER_HAS_FILM\" (" + //
                "\"_id\" INTEGER PRIMARY KEY AUTOINCREMENT ," + // 0: id
                "\"USER_ID\" INTEGER NOT NULL ," + // 1: userId
                "\"FILM_ID\" INTEGER NOT NULL );"); // 2: filmId
    }

    /** Drops the underlying database table. */
    public static void dropTable(Database db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"USER_HAS_FILM\"";
        db.execSQL(sql);
    }

    @Override
    protected final void bindValues(DatabaseStatement stmt, UserHasFilm entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
        stmt.bindLong(2, entity.getUserId());
        stmt.bindLong(3, entity.getFilmId());
    }

    @Override
    protected final void bindValues(SQLiteStatement stmt, UserHasFilm entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
        stmt.bindLong(2, entity.getUserId());
        stmt.bindLong(3, entity.getFilmId());
    }

    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    @Override
    public UserHasFilm readEntity(Cursor cursor, int offset) {
        UserHasFilm entity = new UserHasFilm( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
            cursor.getLong(offset + 1), // userId
            cursor.getLong(offset + 2) // filmId
        );
        return entity;
    }
     
    @Override
    public void readEntity(Cursor cursor, UserHasFilm entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setUserId(cursor.getLong(offset + 1));
        entity.setFilmId(cursor.getLong(offset + 2));
     }
    
    @Override
    protected final Long updateKeyAfterInsert(UserHasFilm entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    @Override
    public Long getKey(UserHasFilm entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    @Override
    public boolean hasKey(UserHasFilm entity) {
        return entity.getId() != null;
    }

    @Override
    protected final boolean isEntityUpdateable() {
        return true;
    }
    
    /** Internal query to resolve the "filmUsers" to-many relationship of Film. */
    public List<UserHasFilm> _queryFilm_FilmUsers(long filmId) {
        synchronized (this) {
            if (film_FilmUsersQuery == null) {
                QueryBuilder<UserHasFilm> queryBuilder = queryBuilder();
                queryBuilder.where(Properties.FilmId.eq(null));
                film_FilmUsersQuery = queryBuilder.build();
            }
        }
        Query<UserHasFilm> query = film_FilmUsersQuery.forCurrentThread();
        query.setParameter(0, filmId);
        return query.list();
    }

    /** Internal query to resolve the "userFilms" to-many relationship of LoggedUser. */
    public List<UserHasFilm> _queryLoggedUser_UserFilms(long userId) {
        synchronized (this) {
            if (loggedUser_UserFilmsQuery == null) {
                QueryBuilder<UserHasFilm> queryBuilder = queryBuilder();
                queryBuilder.where(Properties.UserId.eq(null));
                loggedUser_UserFilmsQuery = queryBuilder.build();
            }
        }
        Query<UserHasFilm> query = loggedUser_UserFilmsQuery.forCurrentThread();
        query.setParameter(0, userId);
        return query.list();
    }

}
