package com.polsl.reviewme.entity;

import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.ToMany;

import java.util.List;

/**
 * LoggedUser entity to save information in database.
 * Created by Rafał Swoboda on 2017-11-07.
 *
 * @author Rafał Swoboda
 * @version 1.0
 * @since 03.12.2017
 */

@Entity
public class LoggedUser {
    @Id(autoincrement = true)
    private Long id;
    private String login;
    private String mail;
    @ToMany(referencedJoinProperty = "userId")
    private List<UserHasGame> userGames;
    @ToMany(referencedJoinProperty = "userId")
    private List<UserHasBook> userBooks;
    @ToMany(referencedJoinProperty = "userId")
    private List<UserHasFilm> userFilms;
    /**
     * Used to resolve relations
     */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;
    /**
     * Used for active entity operations.
     */
    @Generated(hash = 2060565668)
    private transient LoggedUserDao myDao;

    public LoggedUser(String login, String mail) {
        this.login = login;
        this.mail = mail;
    }

    @Generated(hash = 40500609)
    public LoggedUser(Long id, String login, String mail) {
        this.id = id;
        this.login = login;
        this.mail = mail;
    }

    @Generated(hash = 331065833)
    public LoggedUser() {
    }

    @Override
    public boolean equals(Object loggedUser) {
        if (this.getLogin().equals(((LoggedUser) loggedUser).getLogin()))
            return true;
        return false;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return this.login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getMail() {
        return this.mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 70457132)
    public List<UserHasGame> getUserGames() {
        if (userGames == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            UserHasGameDao targetDao = daoSession.getUserHasGameDao();
            List<UserHasGame> userGamesNew = targetDao
                    ._queryLoggedUser_UserGames(id);
            synchronized (this) {
                if (userGames == null) {
                    userGames = userGamesNew;
                }
            }
        }
        return userGames;
    }

    /**
     * Resets a to-many relationship, making the next get call to query for a fresh result.
     */
    @Generated(hash = 2055701957)
    public synchronized void resetUserGames() {
        userGames = null;
    }

    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 99235426)
    public List<UserHasBook> getUserBooks() {
        if (userBooks == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            UserHasBookDao targetDao = daoSession.getUserHasBookDao();
            List<UserHasBook> userBooksNew = targetDao
                    ._queryLoggedUser_UserBooks(id);
            synchronized (this) {
                if (userBooks == null) {
                    userBooks = userBooksNew;
                }
            }
        }
        return userBooks;
    }

    /**
     * Resets a to-many relationship, making the next get call to query for a fresh result.
     */
    @Generated(hash = 359973372)
    public synchronized void resetUserBooks() {
        userBooks = null;
    }

    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 700794035)
    public List<UserHasFilm> getUserFilms() {
        if (userFilms == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            UserHasFilmDao targetDao = daoSession.getUserHasFilmDao();
            List<UserHasFilm> userFilmsNew = targetDao
                    ._queryLoggedUser_UserFilms(id);
            synchronized (this) {
                if (userFilms == null) {
                    userFilms = userFilmsNew;
                }
            }
        }
        return userFilms;
    }

    /**
     * Resets a to-many relationship, making the next get call to query for a fresh result.
     */
    @Generated(hash = 960265327)
    public synchronized void resetUserFilms() {
        userFilms = null;
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    /**
     * called by internal mechanisms, do not call yourself.
     */
    @Generated(hash = 318524410)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getLoggedUserDao() : null;
    }
}
