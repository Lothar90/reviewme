package com.polsl.reviewme.model;

/**
 * Class for receiving review data from server.
 * Created by Rafał Swoboda on 2017-11-24.
 *
 * @author Rafał Swoboda
 * @version 1.0
 * @since 03.12.2017
 */

public class ReviewData {

    private String user;
    private String review;
    private double rating;
    private int positionId;

    public ReviewData() {
    }

    public ReviewData(String user, String review, double rating, int positionId) {
        this.user = user;
        this.review = review;
        this.rating = rating;
        this.positionId = positionId;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public int getPositionId() {
        return positionId;
    }

    public void setPositionId(int positionId) {
        this.positionId = positionId;
    }
}
