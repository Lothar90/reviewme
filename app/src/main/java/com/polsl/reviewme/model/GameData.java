package com.polsl.reviewme.model;

import com.google.gson.annotations.SerializedName;
import com.polsl.reviewme.entity.Company;
import com.polsl.reviewme.entity.Position;
import com.polsl.reviewme.entity.Publisher;

/**
 * Class for receiving game data from server.
 * Created by Rafał Swoboda on 2017-09-28.
 *
 * @author Rafał Swoboda
 * @version 1.0
 * @since 03.12.2017
 */

public class GameData {
    private Long id;
    @SerializedName("positionId")
    private Position position;
    @SerializedName("publisherId")
    private Publisher publisher;
    @SerializedName("companyId")
    private Company company;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}
