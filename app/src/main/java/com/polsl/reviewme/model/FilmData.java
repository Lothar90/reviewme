package com.polsl.reviewme.model;

import com.google.gson.annotations.SerializedName;
import com.polsl.reviewme.entity.Director;
import com.polsl.reviewme.entity.Position;

/**
 * Class for receiving film data from server.
 * Created by Rafał Swoboda on 2017-09-28.
 *
 * @author Rafał Swoboda
 * @version 1.0
 * @since 03.12.2017
 */

public class FilmData {
    private Long id;
    @SerializedName("positionId")
    private Position position;
    @SerializedName("directorId")
    private Director director;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Director getDirector() {
        return director;
    }

    public void setDirector(Director director) {
        this.director = director;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }
}
