package com.polsl.reviewme.model;

import com.polsl.reviewme.entity.Genre;

/**
 * Class for selecting genres from list.
 * Created by Rafał Swoboda on 2017-11-28.
 *
 * @author Rafał Swoboda
 * @version 1.0
 * @since 03.12.2017
 */

public class SelectGenre {

    private Genre genre;
    private boolean checked;

    public SelectGenre() {
    }

    public SelectGenre(Genre genre, boolean checked) {
        this.genre = genre;
        this.checked = checked;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
