package com.polsl.reviewme.model;

import com.google.gson.annotations.SerializedName;
import com.polsl.reviewme.entity.Author;
import com.polsl.reviewme.entity.Position;

/**
 * Class for receiving book data from server.
 * Created by Rafał Swoboda on 2017-09-28.
 *
 * @author Rafał Swoboda
 * @version 1.0
 * @since 03.12.2017
 */

public class BookData {
    private Long id;
    @SerializedName("positionId")
    private Position position;
    @SerializedName("authorId")
    private Author author;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }
}
