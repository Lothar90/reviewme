package com.polsl.reviewme.model;

import com.google.gson.annotations.SerializedName;
import com.polsl.reviewme.entity.Director;

/**
 * Class with film data to send to server.
 * Created by Rafał Swoboda on 2017-11-28.
 *
 * @author Rafał Swoboda
 * @version 1.0
 * @since 03.12.2017
 */

public class SendFilmData {
    @SerializedName("positionId")
    private SendPositionData position;
    @SerializedName("directorId")
    private Director director;

    public SendFilmData() {
    }

    public SendPositionData getPosition() {
        return position;
    }

    public void setPosition(SendPositionData position) {
        this.position = position;
    }

    public Director getDirector() {
        return director;
    }

    public void setDirector(Director director) {
        this.director = director;
    }
}
