package com.polsl.reviewme.model;

import com.google.gson.annotations.SerializedName;
import com.polsl.reviewme.entity.Genre;

import java.util.List;

/**
 * Class with position data to send to server.
 * Created by Rafał Swoboda on 2017-11-28.
 *
 * @author Rafał Swoboda
 * @version 1.0
 * @since 03.12.2017
 */

public class SendPositionData {
    private String title;
    private long premiere;
    @SerializedName("genreCollection")
    private List<Genre> genres;

    public SendPositionData() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getPremiere() {
        return premiere;
    }

    public void setPremiere(long premiere) {
        this.premiere = premiere;
    }

    public List<Genre> getGenres() {
        return genres;
    }

    public void setGenres(List<Genre> genres) {
        this.genres = genres;
    }
}
