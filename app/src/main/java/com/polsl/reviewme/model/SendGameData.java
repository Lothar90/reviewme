package com.polsl.reviewme.model;

import com.google.gson.annotations.SerializedName;
import com.polsl.reviewme.entity.Company;
import com.polsl.reviewme.entity.Publisher;

/**
 * Class with game data to send to server.
 * Created by Rafał Swoboda on 2017-11-28.
 *
 * @author Rafał Swoboda
 * @version 1.0
 * @since 03.12.2017
 */

public class SendGameData {
    @SerializedName("positionId")
    private SendPositionData position;
    @SerializedName("publisherId")
    private Publisher publisher;
    @SerializedName("companyId")
    private Company company;

    public SendGameData() {
    }

    public SendPositionData getPosition() {
        return position;
    }

    public void setPosition(SendPositionData position) {
        this.position = position;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}
