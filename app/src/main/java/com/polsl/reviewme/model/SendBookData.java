package com.polsl.reviewme.model;

import com.google.gson.annotations.SerializedName;
import com.polsl.reviewme.entity.Author;

/**
 * Class with book data to send to server.
 * Created by Rafał Swoboda on 2017-11-28.
 *
 * @author Rafał Swoboda
 * @version 1.0
 * @since 03.12.2017
 */

public class SendBookData {
    @SerializedName("positionId")
    private SendPositionData position;
    @SerializedName("authorId")
    private Author author;

    public SendBookData() {
    }

    public SendPositionData getPosition() {
        return position;
    }

    public void setPosition(SendPositionData position) {
        this.position = position;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }
}
