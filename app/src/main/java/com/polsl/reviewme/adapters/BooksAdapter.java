package com.polsl.reviewme.adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.polsl.reviewme.R;
import com.polsl.reviewme.ReviewMeApplication;
import com.polsl.reviewme.activity.PositionsToCheckActivity;
import com.polsl.reviewme.api.RESTServiceEndpoints;
import com.polsl.reviewme.api.RetrofitClient;
import com.polsl.reviewme.entity.Book;
import com.polsl.reviewme.entity.BookDao;
import com.polsl.reviewme.entity.DaoSession;
import com.polsl.reviewme.entity.Genre;
import com.polsl.reviewme.entity.LoggedUser;
import com.polsl.reviewme.entity.LoggedUserDao;
import com.polsl.reviewme.entity.Position;
import com.polsl.reviewme.entity.UserHasBook;
import com.polsl.reviewme.entity.UserHasBookDao;
import com.polsl.reviewme.model.ReviewData;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.polsl.reviewme.Constants.LOGGED_USER;

/**
 * Class that handles changes in recycler view with books information.
 * Created by Rafał Swoboda on 2017-09-06.
 *
 * @author Rafał Swoboda
 * @version 1.0
 * @since 03.12.2017
 */

public class BooksAdapter extends RecyclerView.Adapter<BooksAdapter.ViewHolder> {

    /**
     * List of all books from database.
     */
    private List<Book> books;
    /**
     * List of books checked by currently logged user.
     */
    private List<UserHasBook> userBooks;
    /**
     * Parent activity context.
     */
    private Context context;
    /**
     * Currently logged user.
     */
    private LoggedUser loggedUser;
    /**
     * DaoSession for database connection.
     */
    private DaoSession daoSession;
    /**
     * Map of images to show next to positions.
     * Taken from server.
     */
    private Map<Long, Drawable> images;

    /**
     * Class constructor that assigns required values.
     *
     * @param books   list of books.
     * @param context activity context.
     */
    public BooksAdapter(List<Book> books, Context context) {
        this.books = books;
        this.context = context;
        this.daoSession = ((ReviewMeApplication) context.getApplicationContext()).getDaoSession();
        images = new HashMap<>();
        LoggedUserDao loggedUserDao = daoSession.getLoggedUserDao();
        SharedPreferences preferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        for (LoggedUser lu : loggedUserDao.loadAll()) {
            if (lu.getLogin().equals(preferences.getString(LOGGED_USER, null))) {
                loggedUser = lu;
                break;
            }
        }
        this.userBooks = loggedUser.getUserBooks();
    }

    /**
     * Method invoked at the create of view holder.
     *
     * @param parent
     * @param viewType
     * @return created view holder.
     */
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View entryItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.entry_list_item, parent, false);
        return new BooksAdapter.ViewHolder(entryItem);
    }

    /**
     * Method invoked to show element of list.
     *
     * @param holder   view holder.
     * @param position actual position.
     */
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Position book = books.get(position).getPosition();

        if (!images.containsKey(books.get(position).getId())) {
            retrieveImage(books.get(position).getId());
        } else {
            if (images.get(books.get(position).getId()) != null)
                holder.miniatureImageView.setBackground(images.get(books.get(position).getId()));
            else
                holder.miniatureImageView.setBackgroundResource(R.mipmap.ic_no_connection);
        }

        holder.titleTextView.setText(book.getTitle());
        holder.authorTextView.setText(books.get(position).getAuthor(books.get(position).getAuthorId()).getName());
        String date = new SimpleDateFormat("yyyy", Locale.getDefault()).format(new Date(book.getPremiere()));
        holder.dateTextView.setText(date);
        holder.averageRatingBar.setRating((float) book.getAverage_rating());
        List<Genre> genres = book.getGenres();
        StringBuilder genresText = new StringBuilder("");
        for (Genre g : genres) {
            genresText.append(g.getName()).append(" ");
        }
        holder.genresTextView.setText(genresText.toString().trim());

        holder.moreImageView.setOnClickListener(view -> {
            PopupMenu popup = new PopupMenu(context, holder.moreImageView);
            popup.inflate(R.menu.list_item_popup_menu);
            popup.setOnMenuItemClickListener(menuItem -> {
                switch (menuItem.getItemId()) {
                    case R.id.review_position:
                        showReviewDialog(book.getTitle(), book.getId().intValue());
                        break;
                    case R.id.show_reviews:
                        showDialogWithReviews(book.getTitle(), book.getId().intValue());
                        break;
                }
                return false;
            });
            popup.show();
        });

        if (userBooks.contains(new UserHasBook(loggedUser.getId(), books.get(position).getId())))
            holder.checkedImageView.setBackground(ContextCompat.getDrawable(context, R.drawable.ic_remove_from_to_check));
        else
            holder.checkedImageView.setBackground(ContextCompat.getDrawable(context, R.drawable.ic_add_to_check));

        holder.checkedImageView.setOnClickListener(view -> {
            UserHasBook userHasBook = new UserHasBook();
            userHasBook.setUserId(loggedUser.getId());
            userHasBook.setBookId(books.get(holder.getAdapterPosition()).getId());
            UserHasBookDao userHasBookDao = daoSession.getUserHasBookDao();
            if (userBooks.contains(userHasBook)) {
                userHasBookDao.delete(userBooks.get(userBooks.indexOf(userHasBook)));
                userBooks.remove(userHasBook);
                if (context instanceof PositionsToCheckActivity) {
                    if (holder.getAdapterPosition() != -1 && holder.getAdapterPosition() < books.size()) {
                        books.remove(holder.getAdapterPosition());
                        notifyItemRemoved(holder.getAdapterPosition());
                        notifyItemRangeChanged(holder.getAdapterPosition(), getItemCount());
                    }
                } else
                    notifyDataSetChanged();
            } else {
                userHasBookDao.insert(userHasBook);
                userBooks.add(userHasBook);
                notifyDataSetChanged();
            }
        });
    }

    /**
     * Method to retrieve image of position.
     *
     * @param id id of position, which image is being looked for.
     */
    private void retrieveImage(long id) {
        RESTServiceEndpoints endpoints = RetrofitClient.getApiService();
        Call<ResponseBody> getBookImage = endpoints.getBookImage(id);
        getBookImage.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    InputStream input = response.body().byteStream();
                    Drawable d = Drawable.createFromStream(input, String.valueOf(id));
                    images.put(id, d);
                } else {
                    images.put(id, null);
                }
                notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("NO_CONNECTION_ERROR", "Could not reach server");
                images.put(id, null);
            }
        });
    }

    /**
     * Method that returns number of items on the list.
     *
     * @return number of items.
     */
    @Override
    public int getItemCount() {
        return books.size();
    }

    /**
     * Method that shows dialog to review position.
     *
     * @param title      dialog title.
     * @param positionId id of reviewed position.
     */
    private void showReviewDialog(String title, final int positionId) {
        final Dialog reviewDialog = new Dialog(context);
        reviewDialog.setContentView(R.layout.rating_dialog);
        reviewDialog.setTitle("Zrecenzuj " + title);
        final RatingBar ratingBar = reviewDialog.findViewById(R.id.review_rating);
        final EditText reviewText = reviewDialog.findViewById(R.id.review_edit_text);
        Button cancelButton = reviewDialog.findViewById(R.id.cancel_review_button);
        Button acceptButton = reviewDialog.findViewById(R.id.accept_review_button);

        cancelButton.setOnClickListener(view -> {
            Toast.makeText(context, R.string.review_cancel, Toast.LENGTH_SHORT).show();
            reviewDialog.dismiss();
        });

        acceptButton.setOnClickListener(view -> {
            if (reviewText.getText().toString().trim().length() > 9) {
                ReviewData reviewData = new ReviewData();
                reviewData.setPositionId(positionId);
                reviewData.setRating(ratingBar.getRating());
                reviewData.setReview(reviewText.getText().toString().trim());
                reviewData.setUser(loggedUser.getLogin());
                RESTServiceEndpoints endpoints = RetrofitClient.getApiService();
                Call<ResponseBody> sendReview = endpoints.sendReview(reviewData);
                sendReview.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.code() == 200) {
                            Toast.makeText(context, R.string.review_send, Toast.LENGTH_SHORT).show();
                            try {
                                String average = response.body().string();
                                BookDao bookDao = daoSession.getBookDao();
                                for (int i = 0; i < books.size(); i++) {
                                    if (books.get(i).getPosition().getId() == positionId) {
                                        books.get(i).getPosition().setAverage_rating(Double.parseDouble(average));
                                        bookDao.update(books.get(i));
                                        notifyDataSetChanged();
                                        break;
                                    }
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } else if (response.code() == 406)
                            try {
                                Toast.makeText(context, response.errorBody().string(), Toast.LENGTH_SHORT).show();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.d("NO_CONNECTION_ERROR", "Could not reach server");
                        Toast.makeText(context, R.string.no_connection, Toast.LENGTH_SHORT).show();
                    }
                });
                reviewDialog.dismiss();
            } else {
                Toast.makeText(context, R.string.review_length_text, Toast.LENGTH_SHORT).show();
            }
        });
        reviewDialog.show();
    }

    /**
     * Method for showing dialog with position reviews.
     *
     * @param title      dialog title.
     * @param positionId id of position.
     */
    private void showDialogWithReviews(String title, int positionId) {
        final Dialog reviewsDialog = new Dialog(context);
        reviewsDialog.setContentView(R.layout.reviews_dialog);
        reviewsDialog.setTitle("Recenzje " + title);
        Button returnButton = reviewsDialog.findViewById(R.id.reviews_dialog_button);
        RecyclerView reviewsRecyclerView = reviewsDialog.findViewById(R.id.reviews_dialog_recycler_view);
        reviewsRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        reviewsRecyclerView.setAdapter(new ReviewsAdapter(positionId));
        reviewsRecyclerView.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));

        returnButton.setOnClickListener(view -> reviewsDialog.dismiss());
        reviewsDialog.show();
    }

    /**
     * Method that clears books list.
     */
    public void clear() {
        books.clear();
    }

    /**
     * Method that adds items to list.
     */
    public void addAll() {
        books = daoSession.getBookDao().loadAll();
        notifyDataSetChanged();
    }

    /**
     * View holder class that holds all views of one item.
     */
    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView titleTextView;
        TextView authorTextView;
        TextView genresTextView;
        TextView dateTextView;
        ImageView miniatureImageView;
        ImageView checkedImageView;
        ImageView moreImageView;
        RatingBar averageRatingBar;

        /**
         * Constructor that finds required views.
         *
         * @param itemView
         */
        public ViewHolder(View itemView) {
            super(itemView);
            titleTextView = itemView.findViewById(R.id.title_text_view);
            authorTextView = itemView.findViewById(R.id.creator_text_view);
            genresTextView = itemView.findViewById(R.id.genres_text_view);
            dateTextView = itemView.findViewById(R.id.date_text_view);
            miniatureImageView = itemView.findViewById(R.id.miniature_image_view);
            checkedImageView = itemView.findViewById(R.id.checked_icon);
            moreImageView = itemView.findViewById(R.id.more_icon);
            averageRatingBar = itemView.findViewById(R.id.average_rating);
        }

    }
}
