package com.polsl.reviewme.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.polsl.reviewme.activity.PositionListActivity;
import com.polsl.reviewme.activity.PositionsToCheckActivity;
import com.polsl.reviewme.fragments.BooksFragment;
import com.polsl.reviewme.fragments.FilmsFragment;
import com.polsl.reviewme.fragments.GamesFragment;
import com.polsl.reviewme.fragments.UserBooksFragment;
import com.polsl.reviewme.fragments.UserFilmsFragment;
import com.polsl.reviewme.fragments.UserGamesFragment;

/**
 * Adapter class for controlling tabs in tab view.
 * Created by Rafał Swoboda on 2017-09-06.
 *
 * @author Rafał Swoboda
 * @version 1.0
 * @since 03.12.2017
 */

public class EntryListPagerAdapter extends FragmentStatePagerAdapter {

    /**
     * Number of all tabs.
     */
    private int numberOfTabs;
    /**
     * Activity context.
     */
    private Context context;

    /**
     * Constructor of adapter.
     *
     * @param fm           fragment manager.
     * @param numberOfTabs number of tabs.
     * @param context      activity context.
     */
    public EntryListPagerAdapter(FragmentManager fm, int numberOfTabs, Context context) {
        super(fm);
        this.numberOfTabs = numberOfTabs;
        this.context = context;
    }

    /**
     * Method that returns fragment at given position.
     *
     * @param position position at which we look for tab.
     * @return found Fragment.
     */
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                if (context instanceof PositionListActivity)
                    return new FilmsFragment();
                else if (context instanceof PositionsToCheckActivity)
                    return new UserFilmsFragment();
            case 1:
                if (context instanceof PositionListActivity)
                    return new BooksFragment();
                else if (context instanceof PositionsToCheckActivity)
                    return new UserBooksFragment();
            case 2:
                if (context instanceof PositionListActivity)
                    return new GamesFragment();
                else if (context instanceof PositionsToCheckActivity)
                    return new UserGamesFragment();
            default:
                return null;
        }
    }

    /**
     * Method that returns number of tabs.
     *
     * @return number of tabs.
     */
    @Override
    public int getCount() {
        return numberOfTabs;
    }
}
