package com.polsl.reviewme.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.polsl.reviewme.R;
import com.polsl.reviewme.model.SelectGenre;

import java.util.List;

/**
 * Adapter class that shows genres in list.
 * Created by Rafał Swoboda on 2017-11-28.
 *
 * @author Rafał Swoboda
 * @version 1.0
 * @since 03.12.2017
 */

public class GenresAdapter extends RecyclerView.Adapter<GenresAdapter.ViewHolder> {

    /**
     * List of genres from database.
     */
    private List<SelectGenre> genres;

    /**
     * Constructor that takes the list of genres.
     *
     * @param genres
     */
    public GenresAdapter(List<SelectGenre> genres) {
        this.genres = genres;
    }

    /**
     * Method invoked at the create of view holder.
     *
     * @param parent
     * @param viewType
     * @return created view holder.
     */
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View genresDialogItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.genres_dialog_item, parent, false);
        return new GenresAdapter.ViewHolder(genresDialogItem);
    }

    /**
     * Method invoked to show element of list.
     *
     * @param holder   view holder.
     * @param position actual position.
     */
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final SelectGenre selectGenre = genres.get(position);
        holder.genreTextView.setText(selectGenre.getGenre().getName());
        holder.selectedCheckBox.setChecked(selectGenre.isChecked());
        holder.selectedCheckBox.setClickable(false);
        holder.genreDialogItem.setOnClickListener(view -> {
            selectGenre.setChecked(!selectGenre.isChecked());
            notifyItemChanged(holder.getAdapterPosition());
        });
    }

    /**
     * Method that returns number of items on the list.
     *
     * @return number of items.
     */
    @Override
    public int getItemCount() {
        return genres.size();
    }

    /**
     * View holder class that holds all views of one item.
     */
    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView genreTextView;
        CheckBox selectedCheckBox;
        View genreDialogItem;

        /**
         * Constructor that finds required views.
         *
         * @param itemView
         */
        public ViewHolder(View itemView) {
            super(itemView);
            genreTextView = itemView.findViewById(R.id.genre_text_view);
            selectedCheckBox = itemView.findViewById(R.id.genre_check_box);
            genreDialogItem = itemView.findViewById(R.id.genre_dialog_item);
        }

    }
}
