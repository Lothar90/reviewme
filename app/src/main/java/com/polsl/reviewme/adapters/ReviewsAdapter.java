package com.polsl.reviewme.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.polsl.reviewme.R;
import com.polsl.reviewme.api.RESTServiceEndpoints;
import com.polsl.reviewme.api.RetrofitClient;
import com.polsl.reviewme.model.ReviewData;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Adapter class that shows list of reviews.
 * Created by Rafał Swoboda on 2017-11-24.
 *
 * @author Rafał Swoboda
 * @version 1.0
 * @since 03.12.2017
 */

public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.ViewHolder> {

    /**
     * List fof reviews from server.
     */
    private List<ReviewData> reviews;

    /**
     * Constructor that takes id of position which reviews should be shown.
     *
     * @param positionId id of position.
     */
    public ReviewsAdapter(int positionId) {
        reviews = new ArrayList<>();
        RESTServiceEndpoints endpoints = RetrofitClient.getApiService();
        Call<List<ReviewData>> getReviews = endpoints.getReviews(positionId);
        getReviews.enqueue(new Callback<List<ReviewData>>() {
            @Override
            public void onResponse(Call<List<ReviewData>> call, Response<List<ReviewData>> response) {
                if (response.isSuccessful()) {
                    reviews = response.body();
                    notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<List<ReviewData>> call, Throwable t) {
                Log.d("NO_CONNECTION_ERROR", "Could not reach server");
            }
        });
    }

    /**
     * Method invoked at the create of view holder.
     *
     * @param parent
     * @param viewType
     * @return created view holder.
     */
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View reviewsDialogItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.reviews_dialog_item, parent, false);
        return new ReviewsAdapter.ViewHolder(reviewsDialogItem);
    }

    /**
     * Method invoked to show element of list.
     *
     * @param holder   view holder.
     * @param position actual position.
     */
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.reviewTextView.setText(reviews.get(position).getReview());
        holder.userTextView.setText(reviews.get(position).getUser());
        holder.positionRatingBar.setRating((float) reviews.get(position).getRating());
    }

    /**
     * Method that returns number of items on the list.
     *
     * @return number of items.
     */
    @Override
    public int getItemCount() {
        return reviews.size();
    }

    /**
     * View holder class that holds all views of one item.
     */
    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView reviewTextView;
        TextView userTextView;
        RatingBar positionRatingBar;

        /**
         * Constructor that finds required views.
         *
         * @param itemView
         */
        public ViewHolder(View itemView) {
            super(itemView);
            reviewTextView = itemView.findViewById(R.id.reviews_dialog_item_text);
            userTextView = itemView.findViewById(R.id.reviews_dialog_item_user);
            positionRatingBar = itemView.findViewById(R.id.reviews_dialog_item_rating);
        }

    }

}
