package com.polsl.reviewme.fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.polsl.reviewme.R;
import com.polsl.reviewme.ReviewMeApplication;
import com.polsl.reviewme.adapters.GamesAdapter;
import com.polsl.reviewme.entity.DaoSession;
import com.polsl.reviewme.entity.Game;
import com.polsl.reviewme.entity.GameDao;
import com.polsl.reviewme.entity.LoggedUser;
import com.polsl.reviewme.entity.LoggedUserDao;
import com.polsl.reviewme.entity.UserHasGame;

import java.util.ArrayList;
import java.util.List;

import static com.polsl.reviewme.Constants.LOGGED_USER;

/**
 * Fragment to show list of user games.
 * Created by Rafał Swoboda on 2017-11-17.
 *
 * @author Rafał Swoboda
 * @version 1.0
 * @since 03.12.2017
 */

public class UserGamesFragment extends Fragment {

    /**
     * List of user games.
     */
    private List<Game> games;
    /**
     * Currently logged user.
     */
    private LoggedUser loggedUser;

    public UserGamesFragment() {
        // Required empty public constructor
    }

    /**
     * Method invoked at creation of fragment.
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaoSession daoSession = ((ReviewMeApplication) getActivity().getApplication()).getDaoSession();
        LoggedUserDao loggedUserDao = daoSession.getLoggedUserDao();
        SharedPreferences preferences = getActivity().getSharedPreferences(getContext().getPackageName(), Context.MODE_PRIVATE);
        for (LoggedUser lu : loggedUserDao.loadAll()) {
            if (lu.getLogin().equals(preferences.getString(LOGGED_USER, null))) {
                loggedUser = lu;
                break;
            }
        }
        GameDao gameDao = daoSession.getGameDao();
        games = new ArrayList<>();
        for (UserHasGame uhg : loggedUser.getUserGames()) {
            games.add(gameDao.load(uhg.getGameId()));
        }
    }

    /**
     * Method invoked at creation of fragment view.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_games, container, false);
        RecyclerView gamesRecyclerView = rootView.findViewById(R.id.games_fragment_recycler_view);
        SwipeRefreshLayout swipeRefreshLayout = rootView.findViewById(R.id.games_fragment_swipe_refresh);
        swipeRefreshLayout.setEnabled(false);
        gamesRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        gamesRecyclerView.setAdapter(new GamesAdapter(games, getContext()));
        // Inflate the layout for this fragment
        return rootView;
    }
}
