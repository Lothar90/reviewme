package com.polsl.reviewme.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.polsl.reviewme.R;
import com.polsl.reviewme.ReviewMeApplication;
import com.polsl.reviewme.activity.AddPositionActivity;
import com.polsl.reviewme.adapters.BooksAdapter;
import com.polsl.reviewme.entity.Book;
import com.polsl.reviewme.entity.DaoSession;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.polsl.reviewme.api.DataRequests.requestBooks;

/**
 * Fragment with list of books.
 * Created by Rafał Swoboda on 2017-09-06.
 *
 * @author Rafał Swoboda
 * @version 1.0
 * @since 03.12.2017
 */

public class BooksFragment extends Fragment {

    /**
     * List of books.
     */
    private List<Book> books;
    /**
     * RecyclerView that will show list of books.
     */
    private RecyclerView booksRecyclerView;
    /**
     * TextView with information about not found positions.
     */
    private TextView notFoundTextView;
    /**
     * Refresh layout to refresh information.
     */
    private SwipeRefreshLayout booksRefreshLayout;
    /**
     * BooksAdapter to show list.
     */
    private BooksAdapter booksAdapter;
    /**
     * DaoSession to connect with database.
     */
    private DaoSession daoSession;

    public BooksFragment() {
        // Required empty public constructor
    }

    /**
     * Method invoked at the creation of fragment.
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        daoSession = ((ReviewMeApplication) getActivity().getApplication()).getDaoSession();
        books = daoSession.getBookDao().loadAll();
    }

    /**
     * Method invoked when fragment view is being created.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_books, container, false);
        booksRecyclerView = rootView.findViewById(R.id.books_fragment_recycler_view);
        booksRefreshLayout = rootView.findViewById(R.id.books_fragment_swipe_refresh);
        booksRefreshLayout.setOnRefreshListener(() ->
                requestBooks(daoSession, getContext(), booksAdapter, booksRefreshLayout));

        notFoundTextView = rootView.findViewById(R.id.books_fragment_not_found_text);
        notFoundTextView.setOnClickListener(view -> {
            Intent intent = new Intent(getActivity(), AddPositionActivity.class);
            intent.putExtra("parentActivity", "books");
            startActivity(intent);
        });

        booksAdapter = new BooksAdapter(books, getContext());
        if (books.isEmpty()) {
            notFoundTextView.setVisibility(View.VISIBLE);
            booksRecyclerView.setVisibility(View.GONE);
        } else {
            notFoundTextView.setVisibility(View.GONE);
            booksRecyclerView.setVisibility(View.VISIBLE);
            booksRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            booksRecyclerView.setAdapter(booksAdapter);
        }
        // Inflate the layout for this fragment
        return rootView;
    }

    /**
     * Method that handles click on search button.
     *
     * @param searchText text that is searched.
     */
    public void onSearchButtonClick(String searchText) {
        if (searchText.trim().isEmpty()) {
            books = daoSession.getBookDao().loadAll();
            booksAdapter = new BooksAdapter(books, getContext());
            booksRecyclerView.setAdapter(booksAdapter);
            notFoundTextView.setVisibility(View.GONE);
            booksRecyclerView.setVisibility(View.VISIBLE);
            return;
        }
        List<Book> foundBooks = new ArrayList<>();
        for (Book b : books) {
            if (b.getPosition().getTitle().toLowerCase().contains(searchText.toLowerCase()))
                foundBooks.add(b);
        }
        if (foundBooks.isEmpty()) {
            notFoundTextView.setVisibility(View.VISIBLE);
            booksRecyclerView.setVisibility(View.GONE);
        } else {
            notFoundTextView.setVisibility(View.GONE);
            booksRecyclerView.setVisibility(View.VISIBLE);
            booksAdapter = new BooksAdapter(foundBooks, getContext());
            booksRecyclerView.setAdapter(booksAdapter);
        }
    }

    /**
     * Method that handles selected spinner item.
     *
     * @param text selected text.
     */
    public void onSpinnerItemSelected(String text) {
        books = daoSession.getBookDao().loadAll();
        switch (text) {
            case "":
                Collections.sort(books, (book, book1) ->
                        book.getId().compareTo(book1.getId()));
                break;
            case "A-Z":
                Collections.sort(books, (book, book1) ->
                        book.getPosition().getTitle().compareTo(book1.getPosition().getTitle()));
                break;
            case "Z-A":
                Collections.sort(books, (book, book1) ->
                        book1.getPosition().getTitle().compareTo(book.getPosition().getTitle()));
                break;
            case "Ocena malejąco":
                Collections.sort(books, (book, book1) ->
                        Double.compare(book1.getPosition().getAverage_rating(), book.getPosition().getAverage_rating()));
                break;
            case "Ocena rosnąco":
                Collections.sort(books, (book, book1) ->
                        Double.compare(book.getPosition().getAverage_rating(), book1.getPosition().getAverage_rating()));
                break;
            case "Data malejąco":
                Collections.sort(books, (book, book1) ->
                        Long.compare(book1.getPosition().getPremiere(), book.getPosition().getPremiere()));
                break;
            case "Data rosnąco":
                Collections.sort(books, (book, book1) ->
                        Long.compare(book.getPosition().getPremiere(), book1.getPosition().getPremiere()));
                break;
            default:
                break;
        }
        booksAdapter = new BooksAdapter(books, getContext());
        booksRecyclerView.setAdapter(booksAdapter);
        notFoundTextView.setVisibility(View.GONE);
        booksRecyclerView.setVisibility(View.VISIBLE);
    }

}
