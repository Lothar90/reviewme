package com.polsl.reviewme.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.polsl.reviewme.R;
import com.polsl.reviewme.ReviewMeApplication;
import com.polsl.reviewme.activity.AddPositionActivity;
import com.polsl.reviewme.adapters.GamesAdapter;
import com.polsl.reviewme.entity.DaoSession;
import com.polsl.reviewme.entity.Game;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.polsl.reviewme.api.DataRequests.requestGames;

/**
 * Fragment with list of games.
 * Created by Rafał Swoboda on 2017-09-06.
 *
 * @author Rafał Swoboda
 * @version 1.0
 * @since 03.12.2017
 */

public class GamesFragment extends Fragment {

    /**
     * List of games.
     */
    private List<Game> games;
    /**
     * RecyclerView that will show list of games.
     */
    private RecyclerView gamesRecyclerView;
    /**
     * TextView with information about not found positions.
     */
    private TextView notFoundTextView;
    /**
     * Refresh layout to refresh information.
     */
    private SwipeRefreshLayout gamesRefreshLayout;
    /**
     * GamesAdapter to show list.
     */
    private GamesAdapter gamesAdapter;
    /**
     * DaoSession to connect with database.
     */
    private DaoSession daoSession;

    public GamesFragment() {
        // Required empty public constructor
    }

    /**
     * Method invoked at the creation of fragment.
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        daoSession = ((ReviewMeApplication) getActivity().getApplication()).getDaoSession();
        games = daoSession.getGameDao().loadAll();
    }

    /**
     * Method invoked when fragment view is being created.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_games, container, false);
        gamesRecyclerView = rootView.findViewById(R.id.games_fragment_recycler_view);
        gamesRefreshLayout = rootView.findViewById(R.id.games_fragment_swipe_refresh);
        gamesRefreshLayout.setOnRefreshListener(() ->
                requestGames(daoSession, getContext(), gamesAdapter, gamesRefreshLayout));
        notFoundTextView = rootView.findViewById(R.id.games_fragment_not_found_text);
        notFoundTextView.setOnClickListener(view -> {
            Intent intent = new Intent(getActivity(), AddPositionActivity.class);
            intent.putExtra("parentActivity", "games");
            startActivity(intent);
        });

        if (games.isEmpty()) {
            gamesRecyclerView.setVisibility(View.GONE);
            notFoundTextView.setVisibility(View.VISIBLE);
        } else {
            notFoundTextView.setVisibility(View.GONE);
            gamesRecyclerView.setVisibility(View.VISIBLE);
            gamesRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            gamesAdapter = new GamesAdapter(games, getContext());
            gamesRecyclerView.setAdapter(gamesAdapter);
        }
        // Inflate the layout for this fragment
        return rootView;
    }

    /**
     * Method that handles click on search button.
     *
     * @param searchText text that is searched.
     */
    public void onSearchButtonClick(String searchText) {
        if (searchText.trim().isEmpty()) {
            DaoSession daoSession = ((ReviewMeApplication) getActivity().getApplication()).getDaoSession();
            games = daoSession.getGameDao().loadAll();
            gamesAdapter = new GamesAdapter(games, getContext());
            gamesRecyclerView.setAdapter(gamesAdapter);
            gamesRecyclerView.setVisibility(View.VISIBLE);
            notFoundTextView.setVisibility(View.GONE);
            return;
        }
        List<Game> foundGames = new ArrayList<>();
        for (Game g : games) {
            if (g.getPosition().getTitle().toLowerCase().contains(searchText.toLowerCase()))
                foundGames.add(g);
        }
        if (foundGames.isEmpty()) {
            notFoundTextView.setVisibility(View.VISIBLE);
            gamesRecyclerView.setVisibility(View.GONE);
        } else {
            gamesRecyclerView.setVisibility(View.VISIBLE);
            notFoundTextView.setVisibility(View.GONE);
            gamesAdapter = new GamesAdapter(foundGames, getContext());
            gamesRecyclerView.setAdapter(gamesAdapter);
        }
    }

    /**
     * Method that handles selected spinner item.
     *
     * @param text selected text.
     */
    public void onSpinnerItemSelected(String text) {
        games = daoSession.getGameDao().loadAll();
        switch (text) {
            case "":
                Collections.sort(games, (game, game1) ->
                        game.getId().compareTo(game1.getId()));
                break;
            case "A-Z":
                Collections.sort(games, (game, game1) ->
                        game.getPosition().getTitle().compareTo(game1.getPosition().getTitle()));
                break;
            case "Z-A":
                Collections.sort(games, (game, game1) ->
                        game1.getPosition().getTitle().compareTo(game.getPosition().getTitle()));
                break;
            case "Ocena malejąco":
                Collections.sort(games, (game, game1) ->
                        Double.compare(game1.getPosition().getAverage_rating(), game.getPosition().getAverage_rating()));
                break;
            case "Ocena rosnąco":
                Collections.sort(games, (game, game1) ->
                        Double.compare(game.getPosition().getAverage_rating(), game1.getPosition().getAverage_rating()));
                break;
            case "Data malejąco":
                Collections.sort(games, (game, game1) ->
                        Long.compare(game1.getPosition().getPremiere(), game.getPosition().getPremiere()));
                break;
            case "Data rosnąco":
                Collections.sort(games, (game, game1) ->
                        Long.compare(game.getPosition().getPremiere(), game1.getPosition().getPremiere()));
                break;
            default:
                break;
        }
        gamesAdapter = new GamesAdapter(games, getContext());
        gamesRecyclerView.setAdapter(gamesAdapter);
        gamesRecyclerView.setVisibility(View.VISIBLE);
        notFoundTextView.setVisibility(View.GONE);
    }
}
