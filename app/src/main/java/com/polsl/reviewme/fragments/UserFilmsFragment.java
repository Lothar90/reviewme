package com.polsl.reviewme.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.polsl.reviewme.R;
import com.polsl.reviewme.ReviewMeApplication;
import com.polsl.reviewme.adapters.FilmsAdapter;
import com.polsl.reviewme.entity.DaoSession;
import com.polsl.reviewme.entity.Film;
import com.polsl.reviewme.entity.FilmDao;
import com.polsl.reviewme.entity.LoggedUser;
import com.polsl.reviewme.entity.LoggedUserDao;
import com.polsl.reviewme.entity.UserHasFilm;

import java.util.ArrayList;
import java.util.List;

import static com.polsl.reviewme.Constants.LOGGED_USER;

/**
 * Fragment to show list of user films.
 * Created by Rafał Swoboda on 2017-11-17.
 *
 * @author Rafał Swoboda
 * @version 1.0
 * @since 03.12.2017
 */

public class UserFilmsFragment extends Fragment {

    /**
     * List of user films.
     */
    private List<Film> films;
    /**
     * Currently logged user.
     */
    private LoggedUser loggedUser;

    public UserFilmsFragment() {
        // Required empty public constructor
    }

    /**
     * Method invoked at creation of fragment.
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaoSession daoSession = ((ReviewMeApplication) getActivity().getApplication()).getDaoSession();
        LoggedUserDao loggedUserDao = daoSession.getLoggedUserDao();
        SharedPreferences preferences = getActivity().getSharedPreferences(getContext().getPackageName(), Context.MODE_PRIVATE);
        for (LoggedUser lu : loggedUserDao.loadAll()) {
            if (lu.getLogin().equals(preferences.getString(LOGGED_USER, null))) {
                loggedUser = lu;
                break;
            }
        }
        FilmDao filmDao = daoSession.getFilmDao();
        films = new ArrayList<>();
        for (UserHasFilm uhf : loggedUser.getUserFilms()) {
            films.add(filmDao.load(uhf.getFilmId()));
        }
    }

    /**
     * Method invoked at creation of fragment view.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_films, container, false);
        RecyclerView filmsRecyclerView = rootView.findViewById(R.id.films_fragment_recycler_view);
        SwipeRefreshLayout swipeRefreshLayout = rootView.findViewById(R.id.films_fragment_swipe_refresh);
        swipeRefreshLayout.setEnabled(false);
        filmsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        filmsRecyclerView.setAdapter(new FilmsAdapter(films, getContext()));
        // Inflate the layout for this fragment
        return rootView;
    }
}
