package com.polsl.reviewme.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.polsl.reviewme.R;
import com.polsl.reviewme.ReviewMeApplication;
import com.polsl.reviewme.activity.AddPositionActivity;
import com.polsl.reviewme.adapters.FilmsAdapter;
import com.polsl.reviewme.entity.DaoSession;
import com.polsl.reviewme.entity.Film;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.polsl.reviewme.api.DataRequests.requestFilms;

/**
 * Fragment with list of films.
 * Created by Rafał Swoboda on 2017-09-06.
 *
 * @author Rafał Swoboda
 * @version 1.0
 * @since 03.12.2017
 */

public class FilmsFragment extends Fragment {

    /**
     * List of films.
     */
    private List<Film> films;
    /**
     * RecyclerView that will show list of films.
     */
    private RecyclerView filmsRecyclerView;
    /**
     * TextView with information about not found positions.
     */
    private TextView notFoundTextView;
    /**
     * Refresh layout to refresh information.
     */
    private SwipeRefreshLayout filmsRefreshLayout;
    /**
     * FilmsAdapter to show list.
     */
    private FilmsAdapter filmsAdapter;
    /**
     * DaoSession to connect with database.
     */
    private DaoSession daoSession;

    public FilmsFragment() {
        // Required empty public constructor
    }

    /**
     * Method invoked at the creation of fragment.
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        daoSession = ((ReviewMeApplication) getActivity().getApplication()).getDaoSession();
        films = daoSession.getFilmDao().loadAll();
    }

    /**
     * Method invoked when fragment view is being created.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_films, container, false);
        filmsRecyclerView = rootView.findViewById(R.id.films_fragment_recycler_view);
        filmsRefreshLayout = rootView.findViewById(R.id.films_fragment_swipe_refresh);
        filmsRefreshLayout.setOnRefreshListener(() ->
                requestFilms(daoSession, getContext(), filmsAdapter, filmsRefreshLayout));
        notFoundTextView = rootView.findViewById(R.id.films_fragment_not_found_text);
        notFoundTextView.setOnClickListener(view -> {
            Intent intent = new Intent(getActivity(), AddPositionActivity.class);
            intent.putExtra("parentActivity", "films");
            startActivity(intent);
        });

        filmsAdapter = new FilmsAdapter(films, getContext());
        if (films.isEmpty()) {
            notFoundTextView.setVisibility(View.VISIBLE);
            filmsRecyclerView.setVisibility(View.GONE);
        } else {
            notFoundTextView.setVisibility(View.GONE);
            filmsRecyclerView.setVisibility(View.VISIBLE);
            filmsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            filmsRecyclerView.setAdapter(filmsAdapter);
        }
        // Inflate the layout for this fragment
        return rootView;
    }

    /**
     * Method that handles click on search button.
     *
     * @param searchText text that is searched.
     */
    public void onSearchButtonClick(String searchText) {
        if (searchText.trim().isEmpty()) {
            films = daoSession.getFilmDao().loadAll();
            filmsAdapter = new FilmsAdapter(films, getContext());
            filmsRecyclerView.setAdapter(filmsAdapter);
            notFoundTextView.setVisibility(View.GONE);
            filmsRecyclerView.setVisibility(View.VISIBLE);
            return;
        }
        List<Film> foundFilms = new ArrayList<>();
        for (Film f : films) {
            if (f.getPosition().getTitle().toLowerCase().contains(searchText.toLowerCase()))
                foundFilms.add(f);
        }
        if (foundFilms.isEmpty()) {
            notFoundTextView.setVisibility(View.VISIBLE);
            filmsRecyclerView.setVisibility(View.GONE);
        } else {
            notFoundTextView.setVisibility(View.GONE);
            filmsRecyclerView.setVisibility(View.VISIBLE);
            filmsAdapter = new FilmsAdapter(foundFilms, getContext());
            filmsRecyclerView.setAdapter(filmsAdapter);
        }
    }

    /**
     * Method that handles selected spinner item.
     *
     * @param text selected text.
     */
    public void onSpinnerItemSelected(String text) {
        films = daoSession.getFilmDao().loadAll();
        switch (text) {
            case "":
                Collections.sort(films, (film, film1) ->
                        film.getId().compareTo(film1.getId()));
                break;
            case "A-Z":
                Collections.sort(films, (film, film1) ->
                        film.getPosition().getTitle().compareTo(film1.getPosition().getTitle()));
                break;
            case "Z-A":
                Collections.sort(films, (film, film1) ->
                        film1.getPosition().getTitle().compareTo(film.getPosition().getTitle()));
                break;
            case "Ocena malejąco":
                Collections.sort(films, (film, film1) ->
                        Double.compare(film1.getPosition().getAverage_rating(), film.getPosition().getAverage_rating()));
                break;
            case "Ocena rosnąco":
                Collections.sort(films, (film, film1) ->
                        Double.compare(film.getPosition().getAverage_rating(), film1.getPosition().getAverage_rating()));
                break;
            case "Data malejąco":
                Collections.sort(films, (film, film1) ->
                        Long.compare(film1.getPosition().getPremiere(), film.getPosition().getPremiere()));
                break;
            case "Data rosnąco":
                Collections.sort(films, (film, film1) ->
                        Long.compare(film.getPosition().getPremiere(), film1.getPosition().getPremiere()));
                break;
            default:
                break;
        }
        filmsAdapter = new FilmsAdapter(films, getContext());
        filmsRecyclerView.setAdapter(filmsAdapter);
        notFoundTextView.setVisibility(View.GONE);
        filmsRecyclerView.setVisibility(View.VISIBLE);
    }

}
