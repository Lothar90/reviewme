package com.polsl.reviewme;

import android.app.Application;
import android.content.SharedPreferences;

import com.crashlytics.android.Crashlytics;
import com.polsl.reviewme.entity.DaoMaster;
import com.polsl.reviewme.entity.DaoSession;

import org.greenrobot.greendao.database.Database;

import io.fabric.sdk.android.Fabric;

import static com.polsl.reviewme.Constants.DATA_TAKEN;
import static com.polsl.reviewme.api.DataRequests.requestAuthors;
import static com.polsl.reviewme.api.DataRequests.requestCompanies;
import static com.polsl.reviewme.api.DataRequests.requestDirectors;
import static com.polsl.reviewme.api.DataRequests.requestGenres;
import static com.polsl.reviewme.api.DataRequests.requestPublishers;

/**
 * Main application class.
 * Created by Rafał Swoboda on 2017-09-06.
 *
 * @author Rafał Swoboda
 * @version 1.0
 * @since 03.12.2017
 */

public class ReviewMeApplication extends Application {

    /**
     * DaoSession to connect with database.
     */
    private DaoSession daoSession;

    /**
     * Method invoked at the creation of the application.
     */
    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "review-me-db");
        Database db = helper.getWritableDb();
        daoSession = new DaoMaster(db).newSession();
        SharedPreferences sharedPreferences = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        boolean inserted = sharedPreferences.getBoolean(DATA_TAKEN, false);
        if (inserted)
            synchronizeDatabase();
    }

    /**
     * Method that calls for data refresh.
     */
    private void synchronizeDatabase() {
        requestAuthors(daoSession);
        requestGenres(daoSession);
        requestCompanies(daoSession);
        requestDirectors(daoSession);
        requestPublishers(daoSession);
    }

    /**
     * DaoSession getter.
     *
     * @return instance of DaoSession.
     */
    public DaoSession getDaoSession() {
        return daoSession;
    }
}
