package com.polsl.reviewme;

/**
 * Class containing constants used in application.
 * Created by Rafał Swoboda on 2017-09-19.
 *
 * @author Rafał Swoboda
 * @version 1.0
 * @since 03.12.2017
 */

public class Constants {
    // Server REST service URL.
    public static final String URL = "http://80.211.198.233:8080/RESTService/res/";
    // Logged user tag.
    public static final String LOGGED_USER = "loggedUser";
    // Data taken tag.
    public static final String DATA_TAKEN = "takenData";
}
