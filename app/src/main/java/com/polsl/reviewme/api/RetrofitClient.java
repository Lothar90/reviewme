package com.polsl.reviewme.api;

import com.polsl.reviewme.Constants;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Class for creating REST client.
 * Created by Rafał Swoboda on 2017-09-19.
 *
 * @author Rafał Swoboda
 * @version 1.0
 * @since 03.12.2017
 */

public class RetrofitClient {

    /**
     * Method that builds REST client.
     *
     * @return REST client.
     */
    private static Retrofit getRetrofitInstance() {
        return new Retrofit.Builder()
                .baseUrl(Constants.URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    /**
     * Method that returns retrofit instance.
     *
     * @return retrofit instance.
     */
    public static RESTServiceEndpoints getApiService() {
        return getRetrofitInstance().create(RESTServiceEndpoints.class);
    }
}
