package com.polsl.reviewme.api;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.widget.Toast;

import com.polsl.reviewme.adapters.BooksAdapter;
import com.polsl.reviewme.adapters.FilmsAdapter;
import com.polsl.reviewme.adapters.GamesAdapter;
import com.polsl.reviewme.entity.Author;
import com.polsl.reviewme.entity.AuthorDao;
import com.polsl.reviewme.entity.Book;
import com.polsl.reviewme.entity.BookDao;
import com.polsl.reviewme.entity.Company;
import com.polsl.reviewme.entity.CompanyDao;
import com.polsl.reviewme.entity.DaoSession;
import com.polsl.reviewme.entity.Director;
import com.polsl.reviewme.entity.DirectorDao;
import com.polsl.reviewme.entity.Film;
import com.polsl.reviewme.entity.FilmDao;
import com.polsl.reviewme.entity.Game;
import com.polsl.reviewme.entity.GameDao;
import com.polsl.reviewme.entity.Genre;
import com.polsl.reviewme.entity.GenreDao;
import com.polsl.reviewme.entity.PositionDao;
import com.polsl.reviewme.entity.PositionHasGenre;
import com.polsl.reviewme.entity.PositionHasGenreDao;
import com.polsl.reviewme.entity.Publisher;
import com.polsl.reviewme.entity.PublisherDao;
import com.polsl.reviewme.model.BookData;
import com.polsl.reviewme.model.FilmData;
import com.polsl.reviewme.model.GameData;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.polsl.reviewme.Constants.DATA_TAKEN;

/**
 * Class that handles data requests from server.
 * Created by Rafał Swoboda on 2017-11-05.
 *
 * @author Rafał Swoboda
 * @version 1.0
 * @since 03.12.2017
 */

public class DataRequests {

    /**
     * Method that requests list of Authors from server.
     *
     * @param daoSession DaoSession to add items to database.
     */
    public static void requestAuthors(final DaoSession daoSession) {
        RESTServiceEndpoints endpoints = RetrofitClient.getApiService();
        Call<List<Author>> authorsCall = endpoints.getAuthors();
        authorsCall.enqueue(new Callback<List<Author>>() {
            @Override
            public void onResponse(Call<List<Author>> call, Response<List<Author>> response) {
                if (response.code() == 200) {
                    AuthorDao authorDao = daoSession.getAuthorDao();
                    List<Author> authorsInDb = authorDao.loadAll();
                    for (Author a : response.body()) {
                        if (authorsInDb.contains(a))
                            authorDao.update(a);
                        else
                            authorDao.insert(a);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Author>> call, Throwable t) {
                Log.d("AUTHORS_ERROR", "Could not reach server.");
            }
        });
    }

    /**
     * Method that requests list of Directors from server.
     *
     * @param daoSession DaoSession to add items to database.
     */
    public static void requestDirectors(final DaoSession daoSession) {
        RESTServiceEndpoints endpoints = RetrofitClient.getApiService();
        Call<List<Director>> directorsCall = endpoints.getDirectors();
        directorsCall.enqueue(new Callback<List<Director>>() {
            @Override
            public void onResponse(Call<List<Director>> call, Response<List<Director>> response) {
                if (response.code() == 200) {
                    DirectorDao directorDao = daoSession.getDirectorDao();
                    List<Director> directorsInDb = directorDao.loadAll();
                    for (Director d : response.body()) {
                        if (directorsInDb.contains(d))
                            directorDao.update(d);
                        else
                            directorDao.insert(d);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Director>> call, Throwable t) {
                Log.d("DIRECTORS_ERROR", "Could not reach server.");
            }
        });
    }


    /**
     * Method that requests list of Companies from server.
     *
     * @param daoSession DaoSession to add items to database.
     */
    public static void requestCompanies(final DaoSession daoSession) {
        RESTServiceEndpoints endpoints = RetrofitClient.getApiService();
        final Call<List<Company>> companiesCall = endpoints.getCompanies();
        companiesCall.enqueue(new Callback<List<Company>>() {
            @Override
            public void onResponse(Call<List<Company>> call, Response<List<Company>> response) {
                if (response.code() == 200) {
                    CompanyDao companyDao = daoSession.getCompanyDao();
                    List<Company> companiesInDb = companyDao.loadAll();
                    for (Company c : response.body()) {
                        if (companiesInDb.contains(c))
                            companyDao.update(c);
                        else
                            companyDao.insert(c);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Company>> call, Throwable t) {
                Log.d("COMPANIES_ERROR", "Could not reach server.");
            }
        });
    }

    /**
     * Method that requests list of Publishers from server.
     *
     * @param daoSession DaoSession to add items to database.
     */
    public static void requestPublishers(final DaoSession daoSession) {
        RESTServiceEndpoints endpoints = RetrofitClient.getApiService();
        Call<List<Publisher>> publishersCall = endpoints.getPublishers();
        publishersCall.enqueue(new Callback<List<Publisher>>() {
            @Override
            public void onResponse(Call<List<Publisher>> call, Response<List<Publisher>> response) {
                if (response.code() == 200) {
                    PublisherDao publisherDao = daoSession.getPublisherDao();
                    List<Publisher> publisherInDb = publisherDao.loadAll();
                    for (Publisher p : response.body()) {
                        if (publisherInDb.contains(p))
                            publisherDao.update(p);
                        else
                            publisherDao.insert(p);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Publisher>> call, Throwable t) {
                Log.d("PUBLISHERS_ERROR", "Could not reach server.");
            }
        });
    }

    /**
     * Method that requests list of Genres from server.
     *
     * @param daoSession DaoSession to add items to database.
     */
    public static void requestGenres(final DaoSession daoSession) {
        RESTServiceEndpoints endpoints = RetrofitClient.getApiService();
        Call<List<Genre>> genresCall = endpoints.getGenres();
        genresCall.enqueue(new Callback<List<Genre>>() {
            @Override
            public void onResponse(Call<List<Genre>> call, Response<List<Genre>> response) {
                if (response.code() == 200) {
                    GenreDao genreDao = daoSession.getGenreDao();
                    List<Genre> genresInDb = genreDao.loadAll();
                    for (Genre g : response.body()) {
                        if (genresInDb.contains(g))
                            genreDao.update(g);
                        else
                            genreDao.insert(g);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Genre>> call, Throwable t) {
                Log.d("GENRES_ERROR", "Could not reach server.");
            }
        });
    }

    /**
     * Method that requests list of Books from server.
     *
     * @param daoSession         DaoSession to add items to database.
     * @param context            activity context.
     * @param booksAdapter       BooksAdapter reference.
     * @param swipeRefreshLayout SwipeRefreshLayout if we want to refresh data.
     */
    public static void requestBooks(final DaoSession daoSession, final Context context, final BooksAdapter booksAdapter, final SwipeRefreshLayout swipeRefreshLayout) {
        RESTServiceEndpoints endpoints = RetrofitClient.getApiService();
        Call<List<BookData>> booksCall = endpoints.getBooks();
        booksCall.enqueue(new Callback<List<BookData>>() {
            @Override
            public void onResponse(Call<List<BookData>> call, Response<List<BookData>> response) {
                if (response.code() == 200) {
                    SharedPreferences preferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
                    boolean inserted = preferences.getBoolean(DATA_TAKEN, false);
                    if (!inserted || (booksAdapter == null && swipeRefreshLayout == null))
                        requestFreshBookData(daoSession, response, context);
                    else {
                        requestBookRefresh(daoSession, response, context);
                        booksAdapter.clear();
                        booksAdapter.addAll();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<BookData>> call, Throwable t) {
                Log.d("BOOKS_ERROR", "Could not reach server.");
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    /**
     * Method that handles request for fresh data.
     *
     * @param daoSession DaoSession to put items to database.
     * @param response   Server response.
     * @param context    Activity context.
     */
    private static void requestFreshBookData(DaoSession daoSession, Response<List<BookData>> response, Context context) {
        BookDao bookDao = daoSession.getBookDao();
        PositionDao positionDao = daoSession.getPositionDao();
        PositionHasGenreDao positionHasGenreDao = daoSession.getPositionHasGenreDao();
        for (BookData b : response.body()) {
            Book book = new Book();
            book.setId(b.getId());
            book.setAuthorId(b.getAuthor().getId());
            book.setPosition(b.getPosition());
            bookDao.insert(book);
            for (Genre g : b.getPosition().getGenres()) {
                PositionHasGenre phg = new PositionHasGenre();
                phg.setPositionId(b.getPosition().getId());
                phg.setGenreId(g.getId());
                positionHasGenreDao.insert(phg);
            }
            positionDao.insert(book.getPosition());
        }
        Toast.makeText(context, "Pobrano książki", Toast.LENGTH_SHORT).show();
    }

    /**
     * Method that handles request for refresh data.
     *
     * @param daoSession DaoSession to put items to database.
     * @param response   Server response.
     * @param context    Activity context.
     */
    private static void requestBookRefresh(DaoSession daoSession, Response<List<BookData>> response, Context context) {
        BookDao bookDao = daoSession.getBookDao();
        PositionDao positionDao = daoSession.getPositionDao();
        PositionHasGenreDao positionHasGenreDao = daoSession.getPositionHasGenreDao();
        List<Book> booksInDb = bookDao.loadAll();
        List<PositionHasGenre> positionGenres = positionHasGenreDao.loadAll();
        for (BookData b : response.body()) {
            for (int i = positionGenres.size() - 1; i >= 0; i--) {
                if (positionGenres.get(i).getPositionId().equals(b.getPosition().getId()))
                    positionHasGenreDao.delete(positionGenres.get(i));
            }
            Book book = new Book();
            book.setId(b.getId());
            book.setAuthorId(b.getAuthor().getId());
            book.setPosition(b.getPosition());
            if (booksInDb.contains(book)) {
                bookDao.update(book);
                for (Genre ge : b.getPosition().getGenres()) {
                    PositionHasGenre phg = new PositionHasGenre();
                    phg.setPositionId(b.getPosition().getId());
                    phg.setGenreId(ge.getId());
                    positionHasGenreDao.insert(phg);
                }
                positionDao.update(book.getPosition());
            } else {
                bookDao.insert(book);
                for (Genre ge : b.getPosition().getGenres()) {
                    PositionHasGenre phg = new PositionHasGenre();
                    phg.setPositionId(b.getPosition().getId());
                    phg.setGenreId(ge.getId());
                    positionHasGenreDao.insert(phg);
                }
                positionDao.insert(book.getPosition());
            }
        }
        Toast.makeText(context, "Zaktualizowano książki", Toast.LENGTH_SHORT).show();
    }

    /**
     * Method that requests list of Films from server.
     *
     * @param daoSession         DaoSession to add items to database.
     * @param context            activity context.
     * @param filmsAdapter       FilmsAdapter reference.
     * @param swipeRefreshLayout SwipeRefreshLayout if we want to refresh data.
     */
    public static void requestFilms(final DaoSession daoSession, final Context context, final FilmsAdapter filmsAdapter, final SwipeRefreshLayout swipeRefreshLayout) {
        RESTServiceEndpoints endpoints = RetrofitClient.getApiService();
        Call<List<FilmData>> filmsCall = endpoints.getFilms();
        filmsCall.enqueue(new Callback<List<FilmData>>() {
            @Override
            public void onResponse(Call<List<FilmData>> call, Response<List<FilmData>> response) {
                if (response.code() == 200) {
                    SharedPreferences preferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
                    boolean inserted = preferences.getBoolean(DATA_TAKEN, false);
                    if (!inserted || (filmsAdapter == null && swipeRefreshLayout == null))
                        requestFreshFilmData(daoSession, response, context);
                    else {
                        requestFilmRefresh(daoSession, response, context);
                        filmsAdapter.clear();
                        filmsAdapter.addAll();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<FilmData>> call, Throwable t) {
                Log.d("FILMS_ERROR", "Could not reach server.");
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    /**
     * Method that handles request for fresh data.
     *
     * @param daoSession DaoSession to put items to database.
     * @param response   Server response.
     * @param context    Activity context.
     */
    private static void requestFreshFilmData(DaoSession daoSession, Response<List<FilmData>> response, Context context) {
        FilmDao filmDao = daoSession.getFilmDao();
        PositionDao positionDao = daoSession.getPositionDao();
        PositionHasGenreDao positionHasGenreDao = daoSession.getPositionHasGenreDao();
        for (FilmData f : response.body()) {
            Film film = new Film();
            film.setId(f.getId());
            film.setDirectorId(f.getDirector().getId());
            film.setPosition(f.getPosition());
            filmDao.insert(film);
            for (Genre g : f.getPosition().getGenres()) {
                PositionHasGenre phg = new PositionHasGenre();
                phg.setPositionId(f.getPosition().getId());
                phg.setGenreId(g.getId());
                positionHasGenreDao.insert(phg);
            }
            positionDao.insert(film.getPosition());
        }
        Toast.makeText(context, "Pobrano filmy", Toast.LENGTH_SHORT).show();
    }

    /**
     * Method that handles request for refresh data.
     *
     * @param daoSession DaoSession to put items to database.
     * @param response   Server response.
     * @param context    Activity context.
     */
    private static void requestFilmRefresh(DaoSession daoSession, Response<List<FilmData>> response, Context context) {
        FilmDao filmDao = daoSession.getFilmDao();
        PositionDao positionDao = daoSession.getPositionDao();
        PositionHasGenreDao positionHasGenreDao = daoSession.getPositionHasGenreDao();
        List<Film> filmsInDb = filmDao.loadAll();
        List<PositionHasGenre> positionGenres = positionHasGenreDao.loadAll();
        for (FilmData f : response.body()) {
            for (int i = positionGenres.size() - 1; i >= 0; i--) {
                if (positionGenres.get(i).getPositionId().equals(f.getPosition().getId()))
                    positionHasGenreDao.delete(positionGenres.get(i));
            }
            Film film = new Film();
            film.setId(f.getId());
            film.setDirectorId(f.getDirector().getId());
            film.setPosition(f.getPosition());
            if (filmsInDb.contains(film)) {
                filmDao.update(film);
                for (Genre ge : f.getPosition().getGenres()) {
                    PositionHasGenre phg = new PositionHasGenre();
                    phg.setPositionId(f.getPosition().getId());
                    phg.setGenreId(ge.getId());
                    positionHasGenreDao.insert(phg);
                }
                positionDao.update(film.getPosition());
            } else {
                filmDao.insert(film);
                for (Genre ge : f.getPosition().getGenres()) {
                    PositionHasGenre phg = new PositionHasGenre();
                    phg.setPositionId(f.getPosition().getId());
                    phg.setGenreId(ge.getId());
                    positionHasGenreDao.insert(phg);
                }
                positionDao.insert(film.getPosition());
            }
        }
        Toast.makeText(context, "Zaktualizowano filmy", Toast.LENGTH_SHORT).show();
    }

    /**
     * Method that requests list of Books from server.
     *
     * @param daoSession         DaoSession to add items to database.
     * @param context            activity context.
     * @param gamesAdapter       GamesAdapter reference.
     * @param swipeRefreshLayout SwipeRefreshLayout if we want to refresh data.
     */
    public static void requestGames(final DaoSession daoSession, final Context context, final GamesAdapter gamesAdapter, final SwipeRefreshLayout swipeRefreshLayout) {
        RESTServiceEndpoints endpoints = RetrofitClient.getApiService();
        Call<List<GameData>> gamesCall = endpoints.getGames();
        gamesCall.enqueue(new Callback<List<GameData>>() {
            @Override
            public void onResponse(Call<List<GameData>> call, Response<List<GameData>> response) {
                if (response.code() == 200) {
                    SharedPreferences preferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
                    boolean inserted = preferences.getBoolean(DATA_TAKEN, false);
                    if (!inserted || (gamesAdapter == null && swipeRefreshLayout == null))
                        requestFreshGameData(daoSession, response, context);
                    else {
                        requestGameRefresh(daoSession, response, context);
                        gamesAdapter.clear();
                        gamesAdapter.addAll();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<GameData>> call, Throwable t) {
                Log.d("GAMES_ERROR", "Could not reach server.");
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    /**
     * Method that handles request for fresh data.
     *
     * @param daoSession DaoSession to put items to database.
     * @param response   Server response.
     * @param context    Activity context.
     */
    private static void requestFreshGameData(DaoSession daoSession, Response<List<GameData>> response, Context context) {
        GameDao gameDao = daoSession.getGameDao();
        PositionDao positionDao = daoSession.getPositionDao();
        PositionHasGenreDao positionHasGenreDao = daoSession.getPositionHasGenreDao();
        for (GameData g : response.body()) {
            Game game = new Game();
            game.setId(g.getId());
            game.setCompanyId(g.getCompany().getId());
            game.setPublisherId(g.getPublisher().getId());
            game.setPosition(g.getPosition());
            gameDao.insert(game);
            for (Genre ge : g.getPosition().getGenres()) {
                PositionHasGenre phg = new PositionHasGenre();
                phg.setPositionId(g.getPosition().getId());
                phg.setGenreId(ge.getId());
                positionHasGenreDao.insert(phg);
            }
            positionDao.insert(game.getPosition());
        }
        Toast.makeText(context, "Pobrano gry", Toast.LENGTH_SHORT).show();
    }

    /**
     * Method that handles request for refresh data.
     *
     * @param daoSession DaoSession to put items to database.
     * @param response   Server response.
     * @param context    Activity context.
     */
    private static void requestGameRefresh(DaoSession daoSession, Response<List<GameData>> response, Context context) {
        GameDao gameDao = daoSession.getGameDao();
        PositionDao positionDao = daoSession.getPositionDao();
        PositionHasGenreDao positionHasGenreDao = daoSession.getPositionHasGenreDao();
        List<Game> gamesInDb = gameDao.loadAll();
        List<PositionHasGenre> positionGenres = positionHasGenreDao.loadAll();
        for (GameData g : response.body()) {
            for (int i = positionGenres.size() - 1; i >= 0; i--) {
                if (positionGenres.get(i).getPositionId().equals(g.getPosition().getId()))
                    positionHasGenreDao.delete(positionGenres.get(i));
            }
            Game game = new Game();
            game.setId(g.getId());
            game.setCompanyId(g.getCompany().getId());
            game.setPublisherId(g.getPublisher().getId());
            game.setPosition(g.getPosition());
            if (gamesInDb.contains(game)) {
                gameDao.update(game);
                for (Genre ge : g.getPosition().getGenres()) {
                    PositionHasGenre phg = new PositionHasGenre();
                    phg.setPositionId(g.getPosition().getId());
                    phg.setGenreId(ge.getId());
                    positionHasGenreDao.insert(phg);
                }
                positionDao.update(game.getPosition());
            } else {
                gameDao.insert(game);
                for (Genre ge : g.getPosition().getGenres()) {
                    PositionHasGenre phg = new PositionHasGenre();
                    phg.setPositionId(g.getPosition().getId());
                    phg.setGenreId(ge.getId());
                    positionHasGenreDao.insert(phg);
                }
                positionDao.insert(game.getPosition());
            }
        }
        Toast.makeText(context, "Zaktualizowano gry", Toast.LENGTH_SHORT).show();
    }
}
