package com.polsl.reviewme.api;

import com.polsl.reviewme.entity.Author;
import com.polsl.reviewme.entity.Company;
import com.polsl.reviewme.entity.Director;
import com.polsl.reviewme.entity.Genre;
import com.polsl.reviewme.entity.Publisher;
import com.polsl.reviewme.model.BookData;
import com.polsl.reviewme.model.FilmData;
import com.polsl.reviewme.model.GameData;
import com.polsl.reviewme.model.ReviewData;
import com.polsl.reviewme.model.SendBookData;
import com.polsl.reviewme.model.SendFilmData;
import com.polsl.reviewme.model.SendGameData;
import com.polsl.reviewme.model.User;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Interface describing REST endpoints.
 * Created by Rafał Swoboda on 2017-09-19.
 *
 * @author Rafał Swoboda
 * @version 1.0
 * @since 03.12.2017
 */

public interface RESTServiceEndpoints {

    @Headers("Content-Type: application/json")
    @POST("user/login")
    Call<ResponseBody> login(@Body User user);

    @Headers("Content-Type: application/json")
    @POST("user/logout")
    Call<ResponseBody> logout();

    @Headers("Content-Type: application/json")
    @POST("user/register")
    Call<ResponseBody> register(@Body User user);

    @Headers("Content-Type: application/json")
    @POST("book")
    Call<List<BookData>> getBooks();

    @Headers("Content-Type: application/json")
    @POST("film")
    Call<List<FilmData>> getFilms();

    @Headers("Content-Type: application/json")
    @POST("game")
    Call<List<GameData>> getGames();

    @Headers("Content-Type: application/json")
    @POST("author")
    Call<List<Author>> getAuthors();

    @Headers("Content-Type: application/json")
    @POST("director")
    Call<List<Director>> getDirectors();

    @Headers("Content-Type: application/json")
    @POST("company")
    Call<List<Company>> getCompanies();

    @Headers("Content-Type: application/json")
    @POST("publisher")
    Call<List<Publisher>> getPublishers();

    @Headers("Content-Type: application/json")
    @POST("genre")
    Call<List<Genre>> getGenres();

    @Headers("Content-Type: application/json")
    @POST("review/send")
    Call<ResponseBody> sendReview(@Body ReviewData reviewData);

    @Headers("Content-Type: application/json")
    @POST("position/reviews/{id}")
    Call<List<ReviewData>> getReviews(@Path("id") int positionId);

    @Headers("Content-Type: application/json")
    @POST("book/proposition")
    Call<ResponseBody> sendBookProposition(@Body SendBookData bookData);

    @Headers("Content-Type: application/json")
    @POST("film/proposition")
    Call<ResponseBody> sendFilmProposition(@Body SendFilmData filmData);

    @Headers("Content-Type: application/json")
    @POST("game/proposition")
    Call<ResponseBody> sendGameProposition(@Body SendGameData gameData);

    @POST("book/image/{id}")
    Call<ResponseBody> getBookImage(@Path("id") long bookId);

    @POST("film/image/{id}")
    Call<ResponseBody> getFilmImage(@Path("id") long filmId);

    @POST("game/image/{id}")
    Call<ResponseBody> getGameImage(@Path("id") long gameId);

}
