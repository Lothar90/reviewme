package com.polsl.reviewme.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.polsl.reviewme.R;
import com.polsl.reviewme.api.RESTServiceEndpoints;
import com.polsl.reviewme.api.RetrofitClient;
import com.polsl.reviewme.model.User;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Activity for registering user.
 * Created by Rafał Swoboda on 2017-09-16.
 *
 * @author Rafał Swoboda
 * @version 1.0
 * @since 03.12.2017
 */

public class RegisterActivity extends AppCompatActivity {

    @BindView(R.id.register_mail_edit_text)
    EditText mailEditText;
    @BindView(R.id.register_login_edit_text)
    EditText loginEditText;
    @BindView(R.id.register_password_edit_text)
    EditText passwordEditText;
    @BindView(R.id.register_confirm_password_edit_text)
    EditText confirmPasswordEditText;

    /**
     * Method invoked at the creation of activity, that assigns required values.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    /**
     * Method for handling click on ActionBar icon.
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Method that handles click on register button.
     * Checks if entered data is correct and registers user.
     *
     * @param view
     */
    public void onRegisterButtonClick(View view) {
        if ((passwordEditText.getText().toString().trim().length() >= 5) && passwordEditText.getText().toString().equals(confirmPasswordEditText.getText().toString())) {
            if (loginEditText.getText().toString().trim().length() >= 3) {
                if (android.util.Patterns.EMAIL_ADDRESS.matcher(mailEditText.getText()).matches()) {
                    User user = new User(loginEditText.getText().toString(), Integer.toString(passwordEditText.getText().toString().hashCode())
                            , Integer.toString(mailEditText.getText().toString().hashCode()));
                    RESTServiceEndpoints endpoints = RetrofitClient.getApiService();
                    Call<ResponseBody> register = endpoints.register(user);
                    register.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            if (response.code() == 200 || response.code() == 204) {
                                Toast.makeText(RegisterActivity.this, R.string.register_successful_text, Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                                startActivity(intent);
                            } else if (response.code() == 401) {
                                try {
                                    Toast.makeText(RegisterActivity.this, response.errorBody().string(), Toast.LENGTH_SHORT).show();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            Log.d("NO_CONNECTION_ERROR", "Could not reach server");
                            Toast.makeText(RegisterActivity.this, R.string.no_connection, Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    Toast.makeText(RegisterActivity.this, R.string.mail_error, Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(RegisterActivity.this, R.string.login_length_text, Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(RegisterActivity.this, R.string.password_error_text, Toast.LENGTH_SHORT).show();
        }
    }
}
