package com.polsl.reviewme.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.polsl.reviewme.R;
import com.polsl.reviewme.adapters.EntryListPagerAdapter;
import com.polsl.reviewme.api.RESTServiceEndpoints;
import com.polsl.reviewme.api.RetrofitClient;
import com.polsl.reviewme.fragments.BooksFragment;
import com.polsl.reviewme.fragments.FilmsFragment;
import com.polsl.reviewme.fragments.GamesFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.polsl.reviewme.Constants.LOGGED_USER;

/**
 * Activity with all positions that user can add to his/her list and review.
 * Created by Rafał Swoboda on 2017-09-06.
 *
 * @author Rafał Swoboda
 * @version 1.0
 * @since 03.12.2017
 */

public class PositionListActivity extends AppCompatActivity {

    @BindView(R.id.entry_list_tabs)
    TabLayout tabLayout;
    @BindView(R.id.entry_list_viewpager)
    ViewPager viewPager;
    @BindView(R.id.entry_list_drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.search_layout)
    LinearLayout searchLayout;
    @BindView(R.id.sort_by_layout)
    LinearLayout sortByLayout;
    @BindView(R.id.entry_list_edit_text_view)
    EditText searchText;
    @BindView(R.id.sort_spinner)
    Spinner sortSpinner;

    /**
     * Ties functionality of DrawerLayout with ActionBar.
     */
    private ActionBarDrawerToggle actionBarDrawerToggle;

    /**
     * Method invoked at the creation of activity, that assigns required values.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_position_list);
        ButterKnife.bind(this);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        prepareNavigationDrawer();

        tabLayout.addTab(tabLayout.newTab().setText("Filmy"));
        tabLayout.addTab(tabLayout.newTab().setText("Książki"));
        tabLayout.addTab(tabLayout.newTab().setText("Gry"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.spinner_strings, android.R.layout.simple_spinner_item);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sortSpinner.setAdapter(spinnerAdapter);
        sortSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                List<Fragment> fragments = getSupportFragmentManager().getFragments();
                for (Fragment f : fragments) {
                    if (f != null && f.getUserVisibleHint()) {
                        if (f instanceof BooksFragment) {
                            ((BooksFragment) f).onSpinnerItemSelected(sortSpinner.getSelectedItem().toString());
                        } else if (f instanceof FilmsFragment) {
                            ((FilmsFragment) f).onSpinnerItemSelected(sortSpinner.getSelectedItem().toString());
                        } else if (f instanceof GamesFragment) {
                            ((GamesFragment) f).onSpinnerItemSelected(sortSpinner.getSelectedItem().toString());
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        EntryListPagerAdapter pagerAdapter = new EntryListPagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount(), this);
        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        viewPager.setPageMargin(5);
        viewPager.setPageMarginDrawable(R.color.colorPrimaryLight);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    /**
     * Method that creates menu on the top of the activity
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_buttons, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Method invoked when user clicks on ActionBar icons.
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (actionBarDrawerToggle.onOptionsItemSelected(item))
            return true;
        switch (item.getItemId()) {
            case R.id.action_search:
                if (searchLayout.getVisibility() == View.GONE) {
                    searchLayout.setVisibility(View.VISIBLE);
                    sortByLayout.setVisibility(View.GONE);
                    searchText.setText("");
                } else if (searchLayout.getVisibility() == View.VISIBLE)
                    searchLayout.setVisibility(View.GONE);
                sortSpinner.setSelection(0);
                return true;

            case R.id.action_sort:
                if (sortByLayout.getVisibility() == View.GONE) {
                    sortByLayout.setVisibility(View.VISIBLE);
                    searchLayout.setVisibility(View.GONE);
                } else if (sortByLayout.getVisibility() == View.VISIBLE)
                    sortByLayout.setVisibility(View.GONE);
                sortSpinner.setSelection(0);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Method that prepares NavigationDrawer to work.
     */
    private void prepareNavigationDrawer() {
        actionBarDrawerToggle = new ActionBarDrawerToggle(this,
                drawerLayout, R.string.app_name, R.string.app_name) {

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };

        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        actionBarDrawerToggle.syncState();
    }

    /**
     * Method for handling click on menu item EntriesToCheck.
     *
     * @param item
     */
    public void onMenuItemEntriesToCheckClick(MenuItem item) {
        Intent intent = new Intent(PositionListActivity.this, PositionsToCheckActivity.class);
        startActivity(intent);
    }

    /**
     * Method for handling click on menu item EntryList.
     *
     * @param item
     */
    public void onMenuItemEntryListClick(MenuItem item) {
        Intent intent = new Intent(PositionListActivity.this, PositionListActivity.class);
        startActivity(intent);
    }

    /**
     * Method for handling click on menu item Logout.
     *
     * @param item
     */
    public void onMenuItemLogoutClick(MenuItem item) {
        SharedPreferences prefs = getSharedPreferences(getPackageName(), Context.MODE_PRIVATE);
        String login = prefs.getString(LOGGED_USER, null);
        if (login != null) {
            RESTServiceEndpoints endpoints = RetrofitClient.getApiService();
            Call<ResponseBody> logout = endpoints.logout();
            logout.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.code() == 200) {
                        SharedPreferences prefs = getSharedPreferences(getPackageName(), Context.MODE_PRIVATE);
                        prefs.edit().putString(LOGGED_USER, null).apply();
                        Toast.makeText(PositionListActivity.this, R.string.logout_text, Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(PositionListActivity.this, LoginActivity.class);
                        startActivity(intent);
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.d("NO_CONNECTION_ERROR", "Could not reach server");
                    Toast.makeText(PositionListActivity.this, R.string.no_connection, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    /**
     * Method that handles click on search button.
     *
     * @param v
     */
    public void onSearchButtonClick(View v) {
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        for (Fragment f : fragments) {
            if (f != null && f.getUserVisibleHint()) {
                if (f instanceof BooksFragment) {
                    ((BooksFragment) f).onSearchButtonClick(searchText.getText().toString());
                } else if (f instanceof FilmsFragment) {
                    ((FilmsFragment) f).onSearchButtonClick(searchText.getText().toString());
                } else if (f instanceof GamesFragment) {
                    ((GamesFragment) f).onSearchButtonClick(searchText.getText().toString());
                }
            }
        }
    }

}
