package com.polsl.reviewme.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.polsl.reviewme.R;
import com.polsl.reviewme.adapters.EntryListPagerAdapter;
import com.polsl.reviewme.api.RESTServiceEndpoints;
import com.polsl.reviewme.api.RetrofitClient;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.polsl.reviewme.Constants.LOGGED_USER;

/**
 * Activity with positions selected by user as the ones to check.
 * Created by Rafał Swoboda on 2017-09-06.
 *
 * @author Rafał Swoboda
 * @version 1.0
 * @since 03.12.2017
 */

public class PositionsToCheckActivity extends AppCompatActivity {

    @BindView(R.id.entities_to_check_tabs)
    TabLayout tabLayout;
    @BindView(R.id.entities_to_check_viewpager)
    ViewPager viewPager;
    @BindView(R.id.entries_to_check_drawer_layout)
    DrawerLayout drawerLayout;

    /**
     * Ties functionality of DrawerLayout with ActionBar.
     */
    private ActionBarDrawerToggle actionBarDrawerToggle;

    /**
     * Method invoked at the creation of activity, that assigns required values.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_positions_to_check);
        ButterKnife.bind(this);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        prepareNavigationDrawer();

        tabLayout.addTab(tabLayout.newTab().setText("Filmy"));
        tabLayout.addTab(tabLayout.newTab().setText("Książki"));
        tabLayout.addTab(tabLayout.newTab().setText("Gry"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        EntryListPagerAdapter pagerAdapter = new EntryListPagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount(), this);
        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        viewPager.setPageMargin(5);
        viewPager.setPageMarginDrawable(R.color.colorPrimaryLight);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    /**
     * Method invoked when user clicks on ActionBar icon.
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (actionBarDrawerToggle.onOptionsItemSelected(item))
            return true;
        return super.onOptionsItemSelected(item);
    }

    /**
     * Method that prepares NavigationDrawer to work.
     */
    private void prepareNavigationDrawer() {
        actionBarDrawerToggle = new ActionBarDrawerToggle(this,
                drawerLayout, R.string.app_name, R.string.app_name) {

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };

        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        actionBarDrawerToggle.syncState();
    }

    /**
     * Method for handling click on menu item EntriesToCheck.
     *
     * @param item
     */
    public void onMenuItemEntriesToCheckClick(MenuItem item) {
        Intent intent = new Intent(PositionsToCheckActivity.this, PositionsToCheckActivity.class);
        startActivity(intent);
    }

    /**
     * Method that handles click on menu item EntryList.
     *
     * @param item
     */
    public void onMenuItemEntryListClick(MenuItem item) {
        Intent intent = new Intent(PositionsToCheckActivity.this, PositionListActivity.class);
        startActivity(intent);
    }

    /**
     * Method that handles click on menu item Logout.
     *
     * @param item
     */
    public void onMenuItemLogoutClick(MenuItem item) {
        SharedPreferences prefs = getSharedPreferences(getPackageName(), Context.MODE_PRIVATE);
        String login = prefs.getString(LOGGED_USER, null);
        if (login != null) {
            RESTServiceEndpoints endpoints = RetrofitClient.getApiService();
            Call<ResponseBody> logout = endpoints.logout();
            logout.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.code() == 200) {
                        SharedPreferences prefs = getSharedPreferences(getPackageName(), Context.MODE_PRIVATE);
                        prefs.edit().putString(LOGGED_USER, null).apply();
                        Toast.makeText(PositionsToCheckActivity.this, R.string.logout_text, Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(PositionsToCheckActivity.this, LoginActivity.class);
                        startActivity(intent);
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.d("NO_CONNECTION_ERROR", "Could not reach server");
                    Toast.makeText(PositionsToCheckActivity.this, R.string.no_connection, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
