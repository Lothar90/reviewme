package com.polsl.reviewme.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.polsl.reviewme.R;
import com.polsl.reviewme.ReviewMeApplication;
import com.polsl.reviewme.adapters.GenresAdapter;
import com.polsl.reviewme.api.RESTServiceEndpoints;
import com.polsl.reviewme.api.RetrofitClient;
import com.polsl.reviewme.entity.Author;
import com.polsl.reviewme.entity.AuthorDao;
import com.polsl.reviewme.entity.Company;
import com.polsl.reviewme.entity.CompanyDao;
import com.polsl.reviewme.entity.DaoSession;
import com.polsl.reviewme.entity.Director;
import com.polsl.reviewme.entity.DirectorDao;
import com.polsl.reviewme.entity.Genre;
import com.polsl.reviewme.entity.Publisher;
import com.polsl.reviewme.entity.PublisherDao;
import com.polsl.reviewme.model.SelectGenre;
import com.polsl.reviewme.model.SendBookData;
import com.polsl.reviewme.model.SendFilmData;
import com.polsl.reviewme.model.SendGameData;
import com.polsl.reviewme.model.SendPositionData;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Activity class for adding new position proposition.
 * Created by Rafał Swoboda on 2017-11-16.
 *
 * @author Rafał Swoboda
 * @version 1.0
 * @since 03.12.2017
 */

public class AddPositionActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    @BindView(R.id.add_position_main_text)
    TextView mainTextView;
    @BindView(R.id.add_position_title_edit_text)
    EditText titleEditText;
    @BindView(R.id.add_position_date_text)
    TextView dateTextView;
    @BindView(R.id.add_position_creator_text_view)
    TextView creatorTextView;
    @BindView(R.id.add_position_creator_spinner)
    Spinner creatorSpinner;
    @BindView(R.id.add_position_publisher_view)
    View publisherView;
    @BindView(R.id.add_position_publisher_spinner)
    Spinner publisherSpinner;
    @BindView(R.id.add_position_genres_text)
    TextView genresTextView;
    @BindView(R.id.add_position_send_button)
    Button sendButton;

    /**
     * String describing activity that send request to this activity.
     */
    private String parentActivity;
    /**
     * DaoSession for database connection.
     */
    private DaoSession daoSession;
    /**
     * List of authors from database.
     */
    private List<Author> authors;
    /**
     * List of directors from database.
     */
    private List<Director> directors;
    /**
     * List of publishers from database.
     */
    private List<Publisher> publishers;
    /**
     * List of companies from database.
     */
    private List<Company> companies;
    /**
     * List of genres from database.
     */
    private List<SelectGenre> genres;

    /**
     * Method invoked at the creation of activity, that assigns required values.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_position);
        ButterKnife.bind(this);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        daoSession = ((ReviewMeApplication) getApplication()).getDaoSession();
        long date = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
        String dateString = sdf.format(date);
        dateTextView.setText(dateString);
        genres = new ArrayList<>();
        for (Genre g : daoSession.getGenreDao().loadAll()) {
            SelectGenre selectGenre = new SelectGenre(g, false);
            genres.add(selectGenre);
        }

        parentActivity = getIntent().getStringExtra("parentActivity");
        switch (parentActivity) {
            case "books":
                AuthorDao authorDao = daoSession.getAuthorDao();
                authors = authorDao.loadAll();
                mainTextView.setText(R.string.book_adding);
                creatorTextView.setText(R.string.author_text);
                booksSpinnerUsage();
                break;
            case "films":
                DirectorDao directorDao = daoSession.getDirectorDao();
                directors = directorDao.loadAll();
                mainTextView.setText(R.string.film_adding);
                creatorTextView.setText(R.string.director_text);
                filmsSpinnerUsage();
                break;
            case "games":
                CompanyDao companyDao = daoSession.getCompanyDao();
                PublisherDao publisherDao = daoSession.getPublisherDao();
                companies = companyDao.loadAll();
                publishers = publisherDao.loadAll();
                mainTextView.setText(R.string.game_adding);
                creatorTextView.setText(R.string.company_text);
                publisherView.setVisibility(View.VISIBLE);
                gamesSpinnersUsage();
                break;
            default:
                Intent intent = new Intent(this, PositionListActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }

    /**
     * Method that fills spinner with authors.
     */
    private void booksSpinnerUsage() {
        ArrayAdapter<Author> spinnerAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, authors);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        creatorSpinner.setAdapter(spinnerAdapter);
    }

    /**
     * Method that fills spinner with directors.
     */
    private void filmsSpinnerUsage() {
        ArrayAdapter<Director> spinnerAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, directors);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        creatorSpinner.setAdapter(spinnerAdapter);
    }

    /**
     * Method that fills spinners with companies and publishers.
     */
    private void gamesSpinnersUsage() {
        ArrayAdapter<Company> companySpinnerAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, companies);
        companySpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        creatorSpinner.setAdapter(companySpinnerAdapter);

        ArrayAdapter<Publisher> publisherSpinnerAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, publishers);
        publisherSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        publisherSpinner.setAdapter(publisherSpinnerAdapter);
    }

    /**
     * Method for handling click on button, to choose genres.
     *
     * @param view
     */
    public void onChooseGenresButtonClick(View view) {
        final Dialog genresDialog = new Dialog(this);
        genresDialog.setContentView(R.layout.genres_dialog);
        genresDialog.setTitle(R.string.choose_genres);
        Button acceptButton = genresDialog.findViewById(R.id.genres_dialog_button);
        RecyclerView genresRecyclerView = genresDialog.findViewById(R.id.genres_dialog_recycler_view);
        genresRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        genresRecyclerView.setAdapter(new GenresAdapter(genres));
        genresRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        acceptButton.setOnClickListener(view1 -> {
            StringBuilder genresText = new StringBuilder("");
            for (SelectGenre sg : genres) {
                if (sg.isChecked())
                    genresText.append(sg.getGenre().getName()).append(" ");
            }
            genresTextView.setText(genresText.toString().trim());
            genresDialog.dismiss();
        });
        genresDialog.show();
    }

    /**
     * Mehtod for handling click on Cancel button.
     *
     * @param view
     */
    public void onCancelButtonClick(View view) {
        Intent intent = new Intent(this, PositionListActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * Method for handling click on Send button.
     *
     * @param view
     */
    public void onSendButtonClick(View view) {
        sendButton.setEnabled(false);
        if (titleEditText.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, R.string.title_must_be_entered, Toast.LENGTH_SHORT).show();
            sendButton.setEnabled(true);
        } else {
            try {
                List<Genre> selectedGenres = new ArrayList<>();
                for (SelectGenre sg : genres)
                    if (sg.isChecked())
                        selectedGenres.add(sg.getGenre());
                SendPositionData position = new SendPositionData();
                DateFormat format = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
                Date date = format.parse(dateTextView.getText().toString().trim());
                position.setPremiere(date.getTime());
                position.setTitle(titleEditText.getText().toString().trim());
                position.setGenres(selectedGenres);
                switch (parentActivity) {
                    case "books":
                        prepareAndSendBook(position);
                        break;
                    case "films":
                        prepareAndSendFilm(position);
                        break;
                    case "games":
                        prepareAndSendGame(position);
                        break;
                }
            } catch (ParseException e) {
                e.printStackTrace();
                sendButton.setEnabled(true);
            }
        }
    }

    /**
     * Method that prepares information about book proposition.
     *
     * @param position SendPositionData with partially filled information.
     */
    private void prepareAndSendBook(SendPositionData position) {
        SendBookData bookData = new SendBookData();
        bookData.setPosition(position);
        bookData.setAuthor((Author) creatorSpinner.getSelectedItem());
        RESTServiceEndpoints endpoints = RetrofitClient.getApiService();
        Call<ResponseBody> sendBook = endpoints.sendBookProposition(bookData);
        sendBook.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    Toast.makeText(getApplicationContext(), R.string.book_proposition_send, Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplicationContext(), PositionListActivity.class);
                    startActivity(intent);
                    finish();
                } else if (response.code() == 406) {
                    Toast.makeText(getApplicationContext(), R.string.position_exists, Toast.LENGTH_SHORT).show();
                    sendButton.setEnabled(true);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("NO_CONNECTION_ERROR", "Could not reach server");
                Toast.makeText(getApplicationContext(), R.string.no_connection, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getApplicationContext(), PositionListActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    /**
     * Method that prepares information about film proposition.
     *
     * @param position SendPositionData with partially filled information.
     */
    private void prepareAndSendFilm(SendPositionData position) {
        SendFilmData filmData = new SendFilmData();
        filmData.setPosition(position);
        filmData.setDirector((Director) creatorSpinner.getSelectedItem());
        RESTServiceEndpoints endpoints = RetrofitClient.getApiService();
        Call<ResponseBody> sendFilm = endpoints.sendFilmProposition(filmData);
        sendFilm.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    Toast.makeText(getApplicationContext(), R.string.film_proposition_send, Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplicationContext(), PositionListActivity.class);
                    startActivity(intent);
                    finish();
                } else if (response.code() == 406) {
                    Toast.makeText(getApplicationContext(), R.string.position_exists, Toast.LENGTH_SHORT).show();
                    sendButton.setEnabled(true);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("NO_CONNECTION_ERROR", "Could not reach server");
                Toast.makeText(getApplicationContext(), R.string.no_connection, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getApplicationContext(), PositionListActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    /**
     * Method that prepares information about game proposition.
     *
     * @param position SendPositionData with partially filled information.
     */
    private void prepareAndSendGame(SendPositionData position) {
        SendGameData gameData = new SendGameData();
        gameData.setPosition(position);
        gameData.setCompany((Company) creatorSpinner.getSelectedItem());
        gameData.setPublisher((Publisher) publisherSpinner.getSelectedItem());
        RESTServiceEndpoints endpoints = RetrofitClient.getApiService();
        Call<ResponseBody> sendGame = endpoints.sendGameProposition(gameData);
        sendGame.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    Toast.makeText(getApplicationContext(), R.string.game_proposition_send, Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplicationContext(), PositionListActivity.class);
                    startActivity(intent);
                    finish();
                } else if (response.code() == 406) {
                    Toast.makeText(getApplicationContext(), R.string.position_exists, Toast.LENGTH_SHORT).show();
                    sendButton.setEnabled(true);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("NO_CONNECTION_ERROR", "Could not reach server");
                Toast.makeText(getApplicationContext(), R.string.no_connection, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getApplicationContext(), PositionListActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    /**
     * Method that shows dialog to choose date.
     *
     * @param view
     */
    public void onDatePickButtonClick(View view) {
        final Calendar c = Calendar.getInstance();
        int day = c.get(Calendar.DAY_OF_MONTH);
        int month = c.get(Calendar.MONTH);
        int year = c.get(Calendar.YEAR);
        DatePickerDialog datePickerDialog = new DatePickerDialog(
                this, this, year, month, day);
        datePickerDialog.show();
    }

    /**
     * Method that handles date set on date picker.
     *
     * @param datePicker DatePicker reference.
     * @param year       Set year.
     * @param month      Set month.
     * @param day        Set day.
     */
    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        String dateText = String.format("%02d", day, Locale.getDefault()) + "." +
                String.format("%02d", month + 1, Locale.getDefault()) + "." +
                year;
        dateTextView.setText(dateText.trim());
    }
}
