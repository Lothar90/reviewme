package com.polsl.reviewme.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.polsl.reviewme.R;
import com.polsl.reviewme.ReviewMeApplication;
import com.polsl.reviewme.api.RESTServiceEndpoints;
import com.polsl.reviewme.api.RetrofitClient;
import com.polsl.reviewme.entity.DaoSession;
import com.polsl.reviewme.entity.LoggedUser;
import com.polsl.reviewme.entity.LoggedUserDao;
import com.polsl.reviewme.model.User;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.polsl.reviewme.Constants.DATA_TAKEN;
import static com.polsl.reviewme.Constants.LOGGED_USER;
import static com.polsl.reviewme.api.DataRequests.requestAuthors;
import static com.polsl.reviewme.api.DataRequests.requestBooks;
import static com.polsl.reviewme.api.DataRequests.requestCompanies;
import static com.polsl.reviewme.api.DataRequests.requestDirectors;
import static com.polsl.reviewme.api.DataRequests.requestFilms;
import static com.polsl.reviewme.api.DataRequests.requestGames;
import static com.polsl.reviewme.api.DataRequests.requestGenres;
import static com.polsl.reviewme.api.DataRequests.requestPublishers;

/**
 * Activity that allows user to log in to the application.
 * Created by Rafał Swoboda on 2017-09-16.
 *
 * @author Rafał Swoboda
 * @version 1.0
 * @since 03.12.2017
 */

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.login_edit_text)
    EditText loginEditText;
    @BindView(R.id.password_edit_text)
    EditText passwordEditText;
    private DaoSession daoSession;

    /**
     * Method invoked at the creation of activity, that assigns required values.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        daoSession = ((ReviewMeApplication) getApplication()).getDaoSession();
        checkIfLoggedIn();
    }

    /**
     * Method for handling click on Back.
     * Overridden so it can't go back to application after log out.
     */
    @Override
    public void onBackPressed() {

    }

    /**
     * Method that checks if user is already logged into the application.
     */
    private void checkIfLoggedIn() {
        SharedPreferences prefs = getSharedPreferences(getPackageName(), Context.MODE_PRIVATE);
        String token = prefs.getString(LOGGED_USER, null);
        if (token != null) {
            logUser();
            Intent intent = new Intent(LoginActivity.this, PositionsToCheckActivity.class);
            startActivity(intent);
            finish();
        }
    }

    /**
     * Method that handles click on register text.
     *
     * @param view
     */
    public void onRegisterClick(View view) {
        Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
        startActivity(intent);
    }

    /**
     * Method that handles click on login button.
     * Checks information correctness and logs user in.
     *
     * @param view
     */
    public void onLoginButtonClick(View view) {
        final User userLogin = new User(loginEditText.getText().toString(), Integer.toString(passwordEditText.getText().toString().hashCode()));
        RESTServiceEndpoints endpoints = RetrofitClient.getApiService();
        Call<ResponseBody> login = endpoints.login(userLogin);
        login.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    LoggedUser loggedUser = new LoggedUser(userLogin.getLogin(), userLogin.getMail());
                    LoggedUserDao loggedUserDao = daoSession.getLoggedUserDao();
                    if (!loggedUserDao.loadAll().contains(loggedUser))
                        loggedUserDao.insert(loggedUser);
                    Toast.makeText(LoginActivity.this, R.string.successful_login, Toast.LENGTH_SHORT).show();
                    SharedPreferences prefs = getSharedPreferences(getPackageName(), Context.MODE_PRIVATE);
                    prefs.edit().putString(LOGGED_USER, loginEditText.getText().toString()).apply();
                    logUser();
                    if (!prefs.getBoolean(DATA_TAKEN, false)) {
                        requestDatabaseInformation();
                        prefs.edit().putBoolean(DATA_TAKEN, true).apply();
                    }
                    Intent intent = new Intent(LoginActivity.this, PositionsToCheckActivity.class);
                    startActivity(intent);
                    finish();
                } else if (response.code() == 401) {
                    try {
                        Toast.makeText(LoginActivity.this, response.errorBody().string(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if (response.code() == 404) {
                    try {
                        Toast.makeText(LoginActivity.this, response.errorBody().string(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("NO_CONNECTION_ERROR", "Could not reach server");
                Toast.makeText(LoginActivity.this, R.string.no_connection, Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Method that calls requests to gain information to application.
     */
    private void requestDatabaseInformation() {
        requestGenres(daoSession);
        requestAuthors(daoSession);
        requestDirectors(daoSession);
        requestCompanies(daoSession);
        requestPublishers(daoSession);
        requestBooks(daoSession, this, null, null);
        requestFilms(daoSession, this, null, null);
        requestGames(daoSession, this, null, null);
    }

    /**
     * Method that logs application errors and sends data to external site.
     */
    private void logUser() {
        SharedPreferences prefs = getSharedPreferences(getPackageName(), Context.MODE_PRIVATE);
        String token = prefs.getString(LOGGED_USER, null);
        Crashlytics.setUserIdentifier(token);
        Crashlytics.setUserEmail(token + "@fabric.io");
        Crashlytics.setUserName(token);
    }

}
